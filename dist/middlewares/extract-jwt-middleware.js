"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const User_1 = require("../models/User/User");
exports.extractJwtMiddleware = () => {
    return (req, res, next) => {
        let authorization = req.get('authorization');
        let token = authorization ? authorization.split(' ')[1] : undefined;
        req['context'] = {};
        req['context']['authorization'] = authorization;
        if (!token) {
            return next();
        }
        jwt.verify(token, process.env.KEY_SERVER.trim(), (error, decoded) => {
            if (error) {
                return next();
            }
            User_1.default.findOne({ _id: decoded._id }).then(user => {
                if (user) {
                    req['context']['user'] = {
                        decoded,
                        id: decoded._id,
                        perfil: decoded.perfil._id
                    };
                }
                return next();
            });
        });
    };
};
