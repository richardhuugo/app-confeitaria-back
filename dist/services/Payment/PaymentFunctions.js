"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateCard = (card_info, customer) => {
    let card = JSON.parse(card_info);
    let payload = {
        card_number: card[0].number,
        card_holder_name: card[0].holdername,
        card_expiration_date: card[0].expiration,
        card_cvv: card[0].cvv,
        customer_id: customer
    };
    return payload;
};
exports.generateTransaction = (amount, card_id, customer, cliente, split_rules) => {
    let payload = {
        "amount": amount,
        "card_id": card_id,
        "customer": customer,
        "session": cliente,
        "split_rules": split_rules
    };
    return payload;
};
