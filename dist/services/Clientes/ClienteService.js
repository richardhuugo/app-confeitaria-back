"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../config/constants");
const Perfil_1 = require("../../models/User/Perfil");
const User_1 = require("../../models/User/User");
const utils_1 = require("../../config/utils");
const Cliente_1 = require("../../models/Cliente/Cliente");
const bcrypt = require("bcrypt");
const Documento_1 = require("../../models/Empresa/Documento");
const jwt = require("jsonwebtoken");
exports.registrarCliente = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const email = req.body.email || '';
    const senha = req.body.senha || '';
    User_1.default.findOne({ email }, (error, user) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (user) {
            return res.status(400).send({ message: "Existe um usuario registrado com o email e telefone informado." });
        }
        Perfil_1.default.findOne({ perfil: constants_1.CLIENTE }, (error, perfil) => {
            if (error) {
                return utils_1.sendErrorsFromDB(res, error);
            }
            if (!perfil) {
                return res.status(400).send({ message: "Não conseguimos finalizar o cadastro, entre em contato conosco." });
            }
            const salt = bcrypt.genSaltSync();
            const passwordHash = bcrypt.hashSync(senha, salt);
            const saveUser = new User_1.default({
                nome: '',
                sobrenome: '',
                email,
                senha: passwordHash,
                telefone: '',
                perfil: perfil._id
            });
            saveUser.save(error => {
                if (error) {
                    return utils_1.sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: "Cadastro realizado com sucesso." });
            });
        });
    });
});
exports.alterarSenhaCliente = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const user = req['context']['user'];
    const senhatual = req.body.senha || '';
    const senhanova = req.body.novasenha || '';
    User_1.default.findOne({ _id: user.decoded._id }, (error, usr) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (usr && bcrypt.compareSync(senhatual, usr.senha)) {
            const dadosEmpresa = usr.toObject();
            const salt = bcrypt.genSaltSync();
            const passwordHash = bcrypt.hashSync(senhanova, salt);
            usr.set({ senha: passwordHash });
            usr.save((errorSave, usrSaved) => {
                if (errorSave) {
                    return utils_1.sendErrorsFromDB(res, errorSave);
                }
                Cliente_1.default.findOne({ user: usr._id }, (errorcli, sucesso) => {
                    if (errorcli) {
                        return utils_1.sendErrorsFromDB(res, errorcli);
                    }
                    dadosEmpresa.cliente = sucesso;
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                        expiresIn: "12h"
                    });
                    return res.status(200).send({ token, mensagem: "Senha alterada com sucesso!" });
                });
            });
        }
        else {
            return res.status(400).send({ message: "A senha informada não bate com a atual" });
        }
    });
});
exports.atualizarCliente = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const documento = req.body.documento || '';
    const nome = req.body.nome || '';
    const sobrenome = req.body.sobrenome || '';
    const telefone = req.body.telefone || '';
    const user = req['context']['user'];
    let cliente = yield Cliente_1.default.findOne({ user: user.decoded._id });
    if (cliente) {
        yield atualizar(nome, sobrenome, telefone, user, res);
    }
    else {
        let cli = yield utils_1.client();
        yield novo(cli, nome, sobrenome, documento, user, telefone, res);
    }
});
const atualizar = (nome, sobrenome, telefone, user, res) => __awaiter(this, void 0, void 0, function* () {
    User_1.default.findOne({ _id: user.decoded._id }, (error, success) => {
        if (error) { }
        success.set({ nome, sobrenome, telefone });
        success.save(errorSave => {
            if (errorSave) {
                return utils_1.sendErrorsFromDB(res, errorSave);
            }
            return res.status(200).send({ message: "Dados atualizados com sucesso!." });
        });
    });
});
const novo = (cli, nome, sobrenome, documento, user, telefone, res) => __awaiter(this, void 0, void 0, function* () {
    let doc = yield Documento_1.default.findOne({ documento });
    if (doc) {
        return res.status(400).send({ message: "Não foi possivel concluir o cadastro pois o documento já está registrado no sistema." });
    }
    let newDoct = new Documento_1.default({ tipo: 'cpf', documento });
    User_1.default.findOne({ _id: user.decoded._id }, (errorUsr, userSucesso) => {
        if (errorUsr) {
            return utils_1.sendErrorsFromDB(res, errorUsr);
        }
        userSucesso.set({ nome, sobrenome, telefone });
        userSucesso.save(errorUser => {
            if (errorUser) {
                return utils_1.sendErrorsFromDB(res, errorUser);
            }
            newDoct.save(error => {
                if (error) {
                    return utils_1.sendErrorsFromDB(res, error);
                }
                cli.customers.create({
                    external_id: user.decoded._id,
                    name: nome,
                    type: 'individual',
                    country: 'br',
                    email: user.decoded.email,
                    documents: [
                        {
                            type: 'cpf',
                            number: documento
                        }
                    ],
                    phone_numbers: [telefone]
                }).then(customer => {
                    const saveClient = new Cliente_1.default({
                        pick: '',
                        user: user.decoded._id,
                        custom_id: customer.id,
                        documento: newDoct._id
                    });
                    saveClient.save(error => {
                        if (error) {
                            return utils_1.sendErrorsFromDB(res, error);
                        }
                        return res.status(200).send({ message: "Cadastro realizado com sucesso!" });
                    });
                }).catch(error => {
                    return utils_1.sendErrorsFromDB(res, error.response);
                });
            });
        });
    });
});
