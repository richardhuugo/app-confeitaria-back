"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Empresa_1 = require("../../models/Empresa/Empresa");
const Cupom_1 = require("../../models/Empresa/Cupom");
const Documento_1 = require("../../models/Empresa/Documento");
const User_1 = require("../../models/User/User");
const Perfil_1 = require("../../models/User/Perfil");
const constants_1 = require("../../config/constants");
const utils_1 = require("../../config/utils");
const bcrypt = require("bcrypt");
const Seller_1 = require("../../models/Empresa/Seller");
const Conta_1 = require("../../models/Empresa/Conta");
const ProdutoRetirada_1 = require("../../models/Produto/ProdutoRetirada");
const Pedido_1 = require("../../models/Pedido/Pedido");
exports.registrarEmpresa = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const nickname = req.body.nickname || '';
    const nome_empresa = req.body.nome_empresa || '';
    const documento = req.body.documento || '';
    const descricao = req.body.descricao || '';
    const email = req.body.email || '';
    const senha = req.body.senha || '';
    const telefone = req.body.telefone || '';
    // realiza a verificação do perfil
    let perfil = yield verificarPerfil(constants_1.EMPRESA);
    let usuario = yield verificarUsuario(email);
    let doc = yield verificarDocumento(documento);
    if (!perfil) {
        res.status(400).send({ message: "Perfil não registrado!" });
    }
    else if (usuario) {
        res.status(400).send({ message: "Usuário existente na plataforma!" });
    }
    else if (doc) {
        res.status(400).send({ message: "Documento existente na plataforma!" });
    }
    else {
        const salt = bcrypt.genSaltSync();
        const passwordHash = bcrypt.hashSync(senha, salt);
        const saveUser = new User_1.default({
            nome: '',
            sobrenome: '',
            email,
            senha: passwordHash,
            telefone,
            perfil: perfil._id
        });
        const saveDocumento = new Documento_1.default({
            documento,
            tipo: 'cnpj'
        });
        const saveEmpresa = new Empresa_1.default({
            nickname,
            nome: nome_empresa,
            documento: saveDocumento._id,
            descricao,
            administrador: saveUser._id,
        });
        saveUser.save();
        saveDocumento.save();
        saveEmpresa.save();
        return res.status(200).send({ message: 'Empresa registrada com sucesso! ' });
    }
});
const verificarDocumento = (documento) => __awaiter(this, void 0, void 0, function* () {
    return yield Documento_1.default.findOne({ documento });
});
const verificarPerfil = (perfil) => __awaiter(this, void 0, void 0, function* () {
    return yield Perfil_1.default.findOne({ perfil });
});
const verificarUsuario = (email) => __awaiter(this, void 0, void 0, function* () {
    return yield User_1.default.findOne({ email });
});
exports.registrarContaBancaria = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const { codigobanco, agencia, agencia_dv, conta, conta_dv, documento, nome_titular } = req.body;
    const user = req['context']['user'];
    let cli = yield utils_1.client();
    let contaFind = yield Conta_1.default.findOne({ conta, empresa: user.decoded.empresa._id }, (error, sucess) => __awaiter(this, void 0, void 0, function* () {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (sucess) {
            return res.status(400).send({ message: 'A conta já está criada!' });
        }
        let createConta = yield cli.bankAccounts.create({
            bank_code: codigobanco,
            agencia: agencia,
            agencia_dv: agencia_dv,
            conta: conta,
            conta_dv: conta_dv,
            legal_name: nome_titular,
            document_number: documento
        }).catch(error => { return utils_1.sendErrorsFromDB(res, error.response); });
        yield new Conta_1.default({
            empresa: user.decoded.empresa._id,
            bank_account: createConta.id,
            conta: createConta.conta,
            data: JSON.stringify(createConta)
        }).save(error => {
            if (error) {
                return utils_1.sendErrorsFromDB(res, error);
            }
            return res.status(200).send({ message: "Conta bancaria registrada com sucesso!" });
        });
    }));
});
exports.visualizarSaldo = (req, res) => __awaiter(this, void 0, void 0, function* () {
    let cli = yield utils_1.client();
    const user = req['context']['user'];
    Seller_1.default.findOne({ empresa: user.decoded.empresa._id }, (error, sucesso) => {
        if (error) { }
        if (!sucesso) {
            return res.json({ message: "Atualize seus dados e começe a vender seus produtos." });
        }
        cli.balance.find({
            recipientId: sucesso.recebedor
        }).then(total => {
            return res.status(200).send({ saldo: total });
        }).catch(errorvalor => {
            return utils_1.sendErrorsFromDB(res, errorvalor.response);
        });
    });
});
exports.registrarProdutoRetiradaEndereco = (req, res) => {
    const { produto, endereco } = req.body;
    const user = req['context']['user'];
    new ProdutoRetirada_1.default({
        empresa: user.decoded.empresa._id,
        produto,
        endereco
    }).save(error => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        return res.status(200).send({ message: "Endereço de retirada registrado com sucesso" });
    });
};
exports.registrarSeller = (req, res) => __awaiter(this, void 0, void 0, function* () {
    const { banco_account } = req.body;
    const user = req['context']['user'];
    let cli = yield utils_1.client();
    const seller = yield Seller_1.default.findOne({ empresa: user.decoded.empresa._id });
    if (seller) {
        return res.json({ message: "Função de atualizar dados bancarios do recebedor não implementada" });
    }
    else {
        yield registSeller(cli, banco_account, user, res);
    }
});
const atualizarSeller = () => __awaiter(this, void 0, void 0, function* () {
});
const registSeller = (cli, bank_account, user, res) => __awaiter(this, void 0, void 0, function* () {
    Conta_1.default.findOne({ _id: bank_account, empresa: user.decoded.empresa._id }, (error, sucesso) => __awaiter(this, void 0, void 0, function* () {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (!sucesso) {
            return res.status(400).send({ message: "Não foi possivel registrar o recebedor, informe uma conta bancaria que esteja vinculado ao seu perfil." });
        }
        let recebedor = yield cli.recipients.create({
            bank_account_id: sucesso.bank_account,
            transfer_interval: 'monthly',
            transfer_day: 5,
            transfer_enabled: true
        }).catch(error => {
            return utils_1.sendErrorsFromDB(res, error.response);
        });
        yield new Seller_1.default({
            empresa: user.decoded.empresa._id,
            recebedor: recebedor.id,
            bank_account: sucesso._id,
        }).save(error => {
            console.log(error);
            if (error) {
                return utils_1.sendErrorsFromDB(res, error);
            }
            return res.status(200).send({ message: "Recebedor registrado com sucesso!" });
        });
    }));
});
exports.alterarStatusProdutos = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const user = req['context']['user'];
    const produto = req.body.produto || '';
    const pedido = req.body.pedido || '';
    const status = req.body.status || '';
    yield Pedido_1.default.find({ _id: pedido })
        .select(['_id', 'data_entrega'])
        .populate({ path: 'data_entrega.produto' })
        .exec((error, dados) => __awaiter(this, void 0, void 0, function* () {
        if (user.perfil != constants_1.EMPRESA) {
            return res.json({});
        }
        yield dados.map((ped, indexd) => __awaiter(this, void 0, void 0, function* () {
            yield ped.data_entrega.filter((element, index, array) => __awaiter(this, void 0, void 0, function* () {
                if (new String(element.produto.empresa).valueOf() != new String(user.decoded.empresa._id).valueOf()) {
                    dados.length = 0;
                    dados.push({ error: "Você não tem permissão para alterar um pedido que nao é seu!" });
                }
            }));
        }));
        if (dados.length != 0) {
            Pedido_1.default.findOneAndUpdate({
                'data_entrega.produto': produto
            }, {
                '$set': {
                    'data_entrega.$.status': status
                }
            }, { new: true }, (err, documentoAtualizado) => {
                if (err) {
                    return res.status(400).send(err);
                }
                return res.status(200).send({ message: "Status pedido atualizado com sucesso!" });
            });
        }
        else {
            return res.status(400).send(dados);
        }
    }));
});
exports.registrarCupom = (req, res, next) => {
    const produtos = req.body.produtos || '';
    const cupom = req.body.cupom || '';
    const date_inicio = req.body.date_inicio || '';
    const date_fim = req.body.date_fim || '';
    const quantidade = req.body.quantidade || '';
    const user = req['context']['user'];
    const empresa = user.empresa._id;
    Cupom_1.default.findOne({ cupom }, (error, sucesso) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (sucesso) {
            return res.status(400).send({ message: 'O cupom informado já está registrado! ' });
        }
        new Cupom_1.default({
            produtos: JSON.parse(produtos),
            empresa,
            cupom,
            date_inicio,
            date_fim,
            quantidade
        }).save(error => {
            if (error) {
                return utils_1.sendErrorsFromDB(res, error);
            }
            return res.status(200).send({ message: 'Cupom registrado com sucesso ! ' });
        });
    });
};
exports.adicionarProdutoCupom = (req, res, next) => {
};
