"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const lodash = require("lodash");
const constants_1 = require("../config/constants");
const Perfil_1 = require("../models/User/Perfil");
const utils_1 = require("../config/utils");
const Empresa_1 = require("../models/Empresa/Empresa");
const User_1 = require("../models/User/User");
const UserRoutes_1 = require("../models/userRoutes/UserRoutes");
const Cliente_1 = require("../models/Cliente/Cliente");
const emailRegex = /\S+@\S+\.\S+/;
const passwordRegex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/;
const _ = lodash;
exports.loginUser = (req, res, next) => {
    const email = req.body.email || '';
    const senha = req.body.senha || '';
    User_1.default.findOne({ email })
        .populate('perfil')
        .exec((error, user) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        else if (user && bcrypt.compareSync(senha, user.senha)) {
            const dadosEmpresa = user.toObject();
            //    
            if (user.perfil.perfil === constants_1.EMPRESA) {
                Empresa_1.default.findOne({ administrador: user._id }, (error, sucesso) => {
                    if (error) {
                    }
                    dadosEmpresa.empresa = sucesso;
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                        expiresIn: "12h"
                    });
                    return res.status(200).send({ token, perfil: user.perfil.perfil });
                });
            }
            if (user.perfil.perfil === constants_1.CLIENTE) {
                Cliente_1.default.findOne({ user: user._id }, (error, sucesso) => {
                    if (error) {
                    }
                    dadosEmpresa.cliente = sucesso;
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                        expiresIn: "12h"
                    });
                    return res.status(200).send({ token, perfil: user.perfil.perfil });
                });
            }
        }
        else {
            return res.status(400).send({ errors: ['Usuário/Senha inválidos'] });
        }
    });
};
exports.registrarRoute = (req, res, next) => {
    const route = req.body.route || '';
    const user = req.body.user || '';
    const permission = req.body.permissoes || '';
    UserRoutes_1.default.findOne({ route, user }, (error, sucesso) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        if (sucesso) {
            return res.status(400).send({ message: 'Rota já registrada.' });
        }
        new UserRoutes_1.default({
            route,
            user,
            permission
        }).save(error => {
            if (error) {
                return utils_1.sendErrorsFromDB(res, error);
            }
            return res.status(200).send({ message: 'Rota registrada com sucesso.' });
        });
    });
};
exports.registrarPermission = (req, res, next) => {
};
exports.registrarPerfil = (req, res) => {
    const perfil = req.body.perfil || '';
    Perfil_1.default.findOne({ perfil }, (error, perfilFind) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        else if (perfilFind) {
            return res.status(400).send({ message: 'Perfil já registrado' });
        }
        else {
            new Perfil_1.default({
                perfil
            }).save((error) => {
                if (error) {
                    return utils_1.sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: 'Perfil registrado com sucesso' });
            });
        }
    });
};
