"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
let _ = lodash;
exports.ErrorHandler = (req, res, next) => {
    const bundle = res.locals.bundle;
    if (bundle.errors) {
        const errors = parseErrors(bundle.errors);
        res.status(500).json({ errors, bo: req.body });
    }
    else {
        next();
    }
};
const parseErrors = (nodeRestfulErrors) => {
    const errors = [];
    _.forIn(nodeRestfulErrors, error => errors.push(error.message));
    return errors;
};
