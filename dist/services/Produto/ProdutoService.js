"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Categoria_1 = require("../../models/Produto/Categoria");
const Peso_1 = require("../../models/Produto/Peso");
const Produto_1 = require("../../models/Produto/Produto");
const utils_1 = require("../../config/utils");
const Base64 = require('base-64');
exports.registrarProdutos = (req, res) => {
    const details = req['context']['user'];
    const imagem = req.body.imagens || '';
    const valor = req.body.valor || '';
    const valor_exibir = req.body.valor_exibir || false;
    const descricao = req.body.descricao || '';
    const categoria = req.body.categoria || '';
    const nome = req.body.nome || '';
    const peso = req.body.peso || '';
    const tp_peso = req.body.tp_peso || '';
    const pronta_entrega = req.body.pronta_entrega || '0';
    const saveProduto = new Produto_1.default({
        empresa: details.decoded.empresa._id,
        imagem: JSON.parse(imagem),
        valor,
        valor_exibir,
        pronta_entrega,
        descricao,
        categoria: JSON.parse(categoria),
        nome,
        peso,
        tp_peso
    });
    saveProduto.save(error => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        return res.status(200).send({ message: 'Produto registrado com sucesso !.' });
    });
};
exports.listarProdutos = (req, res) => {
    var id = (req.query.empresa_id) || '';
    var categorias = (req.query.categorias) || '';
    if (id.length != 0) {
        id = Base64.decode(id.trim());
    }
    if (categorias.length != 0) {
        categorias = Base64.decode(categorias.trim());
    }
    let data = {};
    /**  if (id && categorias) {
  
          data = { empresa: id, 'categoria': { $in: ['5c3cc6e91e35162ee07dbd0'] } }
      }
      if (id) {
  
          data = { empresa: id }
      }
      */
    if (categorias) {
        data = { 'categoria': { $in: [categorias] } };
    }
    Produto_1.default.find(data).populate('empresa').populate('categoria').populate('tp_peso').exec((error, sucesso) => {
        if (error) {
            return res.status(400).send(error);
        }
        console.log(sucesso);
        return res.json({ produtos: sucesso });
    });
};
exports.listarCategorias = (req, res) => {
    Categoria_1.default.find({}, (error, categorias) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        return res.json({ categorias });
    });
};
exports.registrarCategoria = (req, res) => {
    const categoria = req.body.categoria || '';
    Categoria_1.default.findOne({ tipo: categoria }, (error, categoriaFind) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        else if (categoriaFind) {
            return res.status(400).send({ message: 'Categoria já registrada no sistema.' });
        }
        else {
            new Categoria_1.default({
                tipo: categoria
            }).save(error => {
                if (error) {
                    return utils_1.sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: 'Categoria registrada com sucesso!' });
            });
        }
    });
};
exports.registrarPeso = (req, res) => {
    const peso = req.body.peso || '';
    Peso_1.default.findOne({ tipo: peso }, (error, pesoFind) => {
        if (error) {
            return utils_1.sendErrorsFromDB(res, error);
        }
        else if (pesoFind) {
            return res.status(200).send({ message: 'O tipo de peso já está registrado!' });
        }
        else {
            new Peso_1.default({ tipo: peso }).save(error => {
                if (error) {
                    return utils_1.sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: 'Tipo de peso registrado com sucesso!' });
            });
        }
    });
};
