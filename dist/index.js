"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./config/server");
require("./config/database");
const routes_1 = require("./config/routes");
routes_1.default(server_1.app);
exports.default = { server: server_1.server };
