"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const config = {
    autoIndex: false,
    useNewUrlParser: true,
};
//export default  mongoose.connect('mongodb://confeitariauser:hugo699177@ds243084.mlab.com:43084/confeitaria' ,config)
exports.default = mongoose.connect('mongodb://localhost/confeitaria', config);
mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório.";
mongoose.Error.messages.Number.min =
    "O '{VALUE}' informado é menor que o limite mínimo de '{MIN}'.";
mongoose.Error.messages.Number.max =
    "O '{VALUE}' informado é maior que o limite máximo de '{MAX}'.";
mongoose.Error.messages.String.enum =
    "'{VALUE}' não é válido para o atributo '{PATH}'.";
