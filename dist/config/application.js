"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const graphsqlHTTP = require("express-graphql");
const bodyParser = require("body-parser");
const queryParser = require("express-query-int");
const logger = require("morgan");
const cors_1 = require("./cors");
const schema_1 = require("../graphql/schema");
const extract_jwt_middleware_1 = require("../middlewares/extract-jwt-middleware");
class Application {
    constructor() {
        this.express = express();
        this.middleware();
    }
    middleware() {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
        this.express.use(cors_1.default);
        this.express.use(queryParser());
        this.express.use(express.static(__dirname + "/public"));
        this.express.use('/graphql', extract_jwt_middleware_1.extractJwtMiddleware(), graphsqlHTTP((req) => ({
            schema: schema_1.default,
            rootValue: global,
            graphiql: process.env.NODE_ENV.trim() === 'development',
            context: req['context']
        })));
    }
}
exports.default = new Application().express;
