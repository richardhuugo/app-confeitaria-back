"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
const bcryptjs_1 = require("bcryptjs");
const lodash = require("lodash");
const _ = lodash;
const pagarme = require("pagarme");
exports.client = () => __awaiter(this, void 0, void 0, function* () {
    return yield pagarme.client.connect({ api_key: (process.env.KEYPAGARME).trim() });
});
exports.isPassword = (encodedPassword, password) => {
    return bcryptjs_1.compareSync(password, encodedPassword);
};
exports.gerarPassword = () => {
    return Math.random().toString(36).slice(-10);
};
exports.transporter = nodemailer.createTransport({
    //host: "gmail.com",
    //port: 25,
    //secure: false, // true for 465, false for other ports
    service: 'Gmail',
    auth: {
        user: "hugorichard2010@gmail.com",
        pass: "qlueqyefwhdziurp"
    },
    tls: { rejectUnauthorized: false }
});
exports.sendErrorsFromDB = (res, dbErrors) => {
    const errors = [];
    _.forIn(dbErrors.errors, error => errors.push(error.message));
    return res.status(400).json({
        errors
    });
};
exports.normalizePort = (val) => {
    let port = (typeof val === 'string') ? parseInt(val) : val;
    if (isNaN(port))
        return val;
    else if (port >= 0)
        return port;
    else
        return false;
};
exports.onError = (server) => {
    return (error) => {
        let port = server.address().port;
        if (error.syscall !== 'listen')
            throw error;
        let bind = (typeof port === 'string') ? `pipe ${port}` : `port ${port}`;
        switch (error.code) {
            case 'EACCES':
                console.error(`${bind} requires elevated privileges`);
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(`${bind} is already in use`);
                process.exit(1);
                break;
            default:
                throw error;
        }
    };
};
exports.onListening = (server) => {
    return () => {
        let addr = server.address();
        let bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
        console.log(`Listening at ${bind}...`);
    };
};
