"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const auth_1 = require("./auth");
const Service_1 = require("../services/Service");
const EmpresaService_1 = require("../services/Empresa/EmpresaService");
const Service_2 = require("../services/Service");
const ProdutoService_1 = require("../services/Produto/ProdutoService");
const ClienteService_1 = require("../services/Clientes/ClienteService");
const PaymentService_1 = require("../services/Payment/PaymentService");
/**const auth = require('./auth')
const servico = require('../services/service');
const adminService = require('../services/Admin/adminService');
const funcionarioService = require('../services/Funcionario/funcionarioService')
const treinoService = require('../services/Treino/treinoService')
const alunoService = require('../services/Aluno/alunoService');
const empresaService = require('../services/Empresa/empresaService')
 */
const routes = (server) => {
    // ROTAS PROTEGIDAS
    const protectedApi = express.Router();
    server.use('/close', protectedApi);
    protectedApi.use(auth_1.default);
    protectedApi.post('/registrar-cupom', EmpresaService_1.registrarCupom);
    protectedApi.post('/registrar-produto', ProdutoService_1.registrarProdutos);
    protectedApi.post('/registrar-status-pedido', PaymentService_1.registrarStatusPedido);
    protectedApi.post('/realizar-pagamento', PaymentService_1.realizarPagamento);
    protectedApi.post('/registrar-seller', EmpresaService_1.registrarSeller);
    protectedApi.post('/atualizar-dados', ClienteService_1.atualizarCliente);
    protectedApi.post('/atualizar-senha', ClienteService_1.alterarSenhaCliente);
    protectedApi.post('/registrar-conta-bancaria', EmpresaService_1.registrarContaBancaria);
    protectedApi.get('/listar-pedidos', PaymentService_1.obterPedidos);
    protectedApi.put('/alterar-status-pedido', EmpresaService_1.alterarStatusProdutos);
    // ROTAS ABERTAS
    const openApi = express.Router();
    server.use('/open', openApi);
    openApi.post('/registrar-perfil', Service_1.registrarPerfil);
    openApi.post('/registrar-empresa', EmpresaService_1.registrarEmpresa);
    openApi.post('/login-user', Service_2.loginUser);
    openApi.post('/registrar-routes', Service_1.registrarRoute);
    openApi.post('/registrar-permissoes', Service_1.registrarPermission);
    openApi.post('/registrar-peso', ProdutoService_1.registrarPeso);
    openApi.post('/registrar-categoria', ProdutoService_1.registrarCategoria);
    openApi.post('/registrar-cliente', ClienteService_1.registrarCliente);
    openApi.get('/listar-produtos', ProdutoService_1.listarProdutos);
    openApi.get('/listar-categorias', ProdutoService_1.listarCategorias);
};
exports.default = routes;
