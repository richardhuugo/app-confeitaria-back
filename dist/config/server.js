"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const io = require("socket.io");
const http = require("http");
const application_1 = require("./application");
exports.app = application_1.default;
const utils_1 = require("./utils");
var server = new http.Server(application_1.default);
exports.server = server;
const socketIo = io(server);
exports.socketIo = socketIo;
const env = process.env.port || 3000;
const port = utils_1.normalizePort(env);
application_1.default.get('/', function (req, res) {
    res.redirect('index.html');
});
socketIo.on('connection', function (socket) {
    socket.on('stream', function (image) {
        socket.broadcast.emit('stream', image);
    });
});
server.listen(port);
server.on('error', utils_1.onError(server));
server.on('listening', utils_1.onListening(server));
