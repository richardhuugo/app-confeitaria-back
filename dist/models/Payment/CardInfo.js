"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let CardInfo = new Schema({
    card: { type: String, required: true },
    cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
    last_digits: { type: String, required: true },
    brand: { type: String, required: true },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('CardInfo', CardInfo);
