"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Payment = new Schema({
    pedido: { type: Schema.Types.ObjectId, ref: 'Pedido' },
    response: { type: String, required: true },
    status_payment: { type: String, required: true },
    payment_id: { type: String, required: true },
}, {
    timestamps: true
});
exports.default = mongoose.model('Payment', Payment);
