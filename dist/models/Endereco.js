"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Endereco = new Schema({
    endereco: { type: String, required: true },
}, {
    timestamps: true
});
exports.default = mongoose.model('Endereco', Endereco);
