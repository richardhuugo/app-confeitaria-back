"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let BlackList = new Schema({
    token: { type: String, required: true },
}, {
    timestamps: true
});
exports.default = mongoose.model('BlackList', BlackList);
