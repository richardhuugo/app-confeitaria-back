"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Permission = new Schema({
    permission: { type: String, required: true },
}, {
    timestamps: true
});
exports.default = mongoose.model('Permission', Permission);
