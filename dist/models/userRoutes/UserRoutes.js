"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let UserRoutes = new Schema({
    route: { type: String, required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    permission: [{ type: Schema.Types.ObjectId, ref: 'Permission' }],
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('UserRoutes', UserRoutes);
