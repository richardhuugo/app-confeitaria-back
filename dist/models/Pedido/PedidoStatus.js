"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let PedidoStatus = new Schema({
    pedido: { type: String, required: true },
    descricao: { type: String, required: true }
});
exports.default = mongoose.model('PedidoStatus', PedidoStatus);
