"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Pedido = new Schema({
    produtos: [{ produto: { type: Schema.Types.ObjectId, ref: 'Produto' }, quantidade: { type: String, required: true }, desconto: { type: String, required: true } }],
    valor_total: { type: String, required: true },
    taxa_servico: { type: String, required: true },
    cliente: { type: Schema.Types.ObjectId, ref: 'Cliente' },
    data_entrega: [{
            produto: { type: Schema.Types.ObjectId, ref: 'Produto' },
            date: { type: Date, required: true },
            status: { type: Schema.Types.ObjectId, ref: 'ProdutoStatus' },
            endereco: { type: Schema.Types.ObjectId, ref: 'Endereco' }
        }],
    status: { type: Schema.Types.ObjectId, ref: 'PedidoStatus' },
}, { timestamps: true });
exports.default = mongoose.model('Pedido', Pedido);
