"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Empresa = new Schema({
    nickname: { type: String, required: false },
    nome: { type: String, required: true },
    documento: { type: Schema.Types.ObjectId, ref: 'Documento' },
    descricao: { type: String, required: false },
    taxa_entrega: { type: String, default: '0' },
    administrador: { type: Schema.Types.ObjectId, ref: 'User' },
    status: { type: String, default: 'I' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Empresa', Empresa);
