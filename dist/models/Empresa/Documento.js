"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Documento = new Schema({
    documento: { type: String, required: true },
    tipo: { type: String, required: true }
}, {
    timestamps: true
});
exports.default = mongoose.model('Documento', Documento);
