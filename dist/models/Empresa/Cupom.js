"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Cupom = new Schema({
    produtos: [{ type: Schema.Types.ObjectId, ref: 'Produto' }],
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    cupom: { type: String, required: true },
    date_inicio: { type: Date, required: true },
    date_fim: { type: Date, required: true },
    quantidade: { type: Number, required: true },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Cupom', Cupom);
