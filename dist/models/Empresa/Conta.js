"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Conta = new Schema({
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    bank_account: { type: String, required: true },
    conta: { type: String, required: true },
    data: { type: String, required: false },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Conta', Conta);
