"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Seller = new Schema({
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    recebedor: { type: String, required: true },
    bank_account: { type: Schema.Types.ObjectId, ref: 'Conta' },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Seller', Seller);
