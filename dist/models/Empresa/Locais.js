"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Locais = new Schema({
    empresa: { type: Schema.Types.ObjectId, ref: 'Documento' },
    endereco: { type: Schema.Types.ObjectId, ref: 'Endereco' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Locais', Locais);
