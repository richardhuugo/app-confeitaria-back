"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Peso = new Schema({
    tipo: { type: String, required: true },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Peso', Peso);
