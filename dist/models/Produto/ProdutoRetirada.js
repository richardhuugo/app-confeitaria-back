"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ProdutoRetirada = new Schema({
    empresa: { type: Schema.Types.ObjectId, ref: 'Empresa' },
    produto: { type: Schema.Types.ObjectId, ref: 'Produto' },
    endereco: { type: String, required: true },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('ProdutoRetirada', ProdutoRetirada);
