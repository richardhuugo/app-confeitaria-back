"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Perfil = new Schema({
    perfil: { type: String, required: true },
    status: { type: String, default: 'A' }
}, {
    timestamps: true
});
exports.default = mongoose.model('Perfil', Perfil);
