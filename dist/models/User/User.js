"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let User = new Schema({
    nome: { type: String, required: false },
    sobrenome: { type: String, required: false },
    email: { type: String, default: true },
    senha: { type: String, required: true },
    telefone: { type: String, required: false },
    perfil: { type: Schema.Types.ObjectId, ref: 'Perfil' },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('User', User);
