"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Cliente = new Schema({
    pick: { type: String, required: false },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    custom_id: { type: String, required: false },
    documento: { type: Schema.Types.ObjectId, ref: 'Documento' },
    status: { type: String, default: 'A' },
}, {
    timestamps: true
});
exports.default = mongoose.model('Cliente', Cliente);
