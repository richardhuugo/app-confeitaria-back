"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const userTypes = `
 

# User definition type
type User {
    _id: ID!
    nome:String!
    sobrenome:String!
    email:String!
    senha:String!
    perfil:Perfil!
}

input UserCreateInput {
    nome:String!
    sobrenome:String!
    email:String!  
    senha:String!             
}

input UserUpdateInput {
    nome:String!
    sobrenome:String!
    senha:String!        
}  

`;
exports.userTypes = userTypes;
const userQueries = `
users(first: Int, offset: Int): [ User! ]!
user(_id: ID!): User

`;
exports.userQueries = userQueries;
const userMutations = `
createUser(input: UserCreateInput!, perfil:[ID!]! ): User
updateUser(_id: ID!, input: UserUpdateInput!): User

`;
exports.userMutations = userMutations;
