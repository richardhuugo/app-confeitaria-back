"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../../models/User/User");
const bcryptjs_1 = require("bcryptjs");
const composable_resolver_1 = require("../../composable/composable.resolver");
const auth_resolver_1 = require("../../composable/auth.resolver");
const verify_token_resolver_1 = require("../../composable/verify-token.resolver");
const prepare = (o) => {
    o._id = o._id.toString();
    return o;
};
exports.userResolvers = {
    //
    Query: {
        users: ((parent, { first = 2, offset = 0 }, context, info) => {
            return User_1.default.find()
                .populate('perfil')
                .select(['nome', 'sobrenome', 'email', '_id'])
                .limit(first)
                .skip(offset)
                .sort({
                tipo: 'asc'
            })
                .then(sucesso => {
                return sucesso;
            });
        }),
        user: composable_resolver_1.compose(auth_resolver_1.authResolver, verify_token_resolver_1.verifyTokenResolver)((parent, { _id }, context, info) => {
            return User_1.default.findOne({ _id })
                .populate('perfil')
                .select(['nome', 'sobrenome', 'email', '_id'])
                .sort({
                tipo: 'asc'
            })
                .then(sucesso => {
                return sucesso;
            });
        })
    },
    Mutation: {
        createUser: (parent, args, context, info) => {
            return User_1.default.findOne({ email: args.input.email }).then((error, sucesso) => {
                if (error) {
                    throw new Error(error);
                }
                if (sucesso) {
                    throw new Error("Existe um usuario registrado com esse email.");
                }
                const salt = bcryptjs_1.genSaltSync();
                const password = bcryptjs_1.hashSync(args.input.senha, salt);
                return new User_1.default({
                    nome: args.input.nome,
                    sobrenome: args.input.sobrenome,
                    email: args.input.email,
                    senha: password,
                    perfil: args.perfil
                }).save().then((error, sucesso) => {
                    if (error) {
                        throw new Error(error);
                    }
                    return { message: 'Usuario registrado com sucesso!' };
                });
            });
        }
    }
};
