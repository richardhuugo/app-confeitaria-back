"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const documentoTypes = `
    type Documento {
        _id:ID!
        documento:String!
        tipo:String!
    }
`;
exports.documentoTypes = documentoTypes;
const documentoQueries = `
documento(_id:String!):Documento!
`;
exports.documentoQueries = documentoQueries;
const documentoMutations = ``;
exports.documentoMutations = documentoMutations;
