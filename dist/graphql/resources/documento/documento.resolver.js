"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Documento_1 = require("../../../models/Empresa/Documento");
exports.documentoResolvers = {
    Query: {
        documento: (parent, { _id }, context, info) => {
            return Documento_1.default.findOne({ _id }).then((documento) => {
                if (!documento) {
                    throw new Error('Nao conseguimos encontrar o documento informado');
                }
                return documento;
            });
        }
    }
};
