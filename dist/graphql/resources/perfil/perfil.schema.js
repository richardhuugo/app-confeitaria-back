"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const perfilTypes = `
 

# User definition type
type Perfil {
    _id: ID!
    perfil:String!
    status:String!
    
}

input PerfilCreateInput {
    perfil:String!
                
}
 

`;
exports.perfilTypes = perfilTypes;
const perfilQueries = `
perfis(first: Int, offset: Int): [ Perfil! ]!
perfil(_id: ID!): Perfil

`;
exports.perfilQueries = perfilQueries;
const perfilMutations = `
createPerfil(input: PerfilCreateInput!): Perfil
delete(_id: ID!): Perfil

`;
exports.perfilMutations = perfilMutations;
