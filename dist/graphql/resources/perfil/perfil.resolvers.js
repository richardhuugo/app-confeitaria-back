"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Perfil_1 = require("../../../models/User/Perfil");
exports.perfillResolvers = {
    Query: {
        perfis: (parent, { first = 2, offset = 0 }, context, info) => {
            return Perfil_1.default.find()
                .limit(first)
                .skip(offset)
                .sort({
                tipo: 'asc'
            })
                .then(sucesso => {
                return sucesso;
            });
        },
        perfil: (parent, { _id }, context, info) => {
            return Perfil_1.default.findOne({ _id }).then((perfil) => {
                if (!perfil) {
                    throw new Error('Perfil não encontrado');
                }
                return perfil;
            });
        }
    },
    Mutation: {
        createPerfil: ((parent, arg, context, info) => {
            return Perfil_1.default.findOne({ perfil: arg.input.perfil }).then(perfil => {
                if (!perfil) {
                    return new Perfil_1.default({ perfil: arg.input.perfil }).save().then(sucesso => {
                        if (!sucesso) {
                            throw new Error('Falha ao salvar perfil');
                        }
                        return { perfil: 'Perfil salvo com sucesso' };
                    });
                }
                return { perfil: 'Não é possivel salvar um perfil que já está cadastrado' };
            });
        }),
        delete: ((parent, {}, context, info) => {
        })
    }
};
