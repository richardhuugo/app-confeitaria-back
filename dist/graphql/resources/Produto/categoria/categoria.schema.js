"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const categoriaTypes = `
# User definition type
type Categoria {
    _id: ID!
   tipo:String!
}

`;
exports.categoriaTypes = categoriaTypes;
const categoriaQueries = `
categorias: [ Categoria! ]!
categoria(_id: ID!): Categoria
`;
exports.categoriaQueries = categoriaQueries;
const categoriaMutations = `
    createCategoria(categoria:String!):Categoria
`;
exports.categoriaMutations = categoriaMutations;
