"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Categoria_1 = require("../../../../models/Produto/Categoria");
exports.categoriaResolvers = {
    Query: {
        categoria: ((parent, { _id }, context, info) => {
            return Categoria_1.default.findOne({ _id })
                .then(categoria => {
                return categoria;
            });
        }),
        // compose(authResolver, verifyTokenResolver)
        categorias: ((parent, args, context, info) => {
            return Categoria_1.default.find()
                .sort({
                tipo: 'asc'
            })
                .then(sucesso => {
                return sucesso;
            });
        })
    },
    Mutation: {
        createCategoria: (parent, { categoria }, context, info) => {
            return Categoria_1.default.findOne({ tipo: categoria }).then((error, sucesso) => {
                if (error) {
                    throw new Error(error);
                }
                if (sucesso) {
                    throw new Error("Essa categoria já se encontra registrada.");
                }
                return new Categoria_1.default({
                    tipo: categoria
                }).save().then((error, sucesso) => {
                    if (error) {
                        throw new Error(error);
                    }
                    return { tipo: 'Categoria registrada com sucesso!' };
                });
            });
        }
    }
};
