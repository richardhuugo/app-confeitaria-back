"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Peso_1 = require("../../../../models/Produto/Peso");
exports.pesoResolvers = {
    Query: {
        peso: ((parent, { _id }, context, info) => {
            return Peso_1.default.findOne({ _id })
                .then(peso => {
                return peso;
            });
        }),
        // compose(authResolver, verifyTokenResolver)
        pesos: ((parent, args, context, info) => {
            return Peso_1.default.find()
                .sort({
                tipo: 'asc'
            })
                .then(peso => {
                return peso;
            });
        })
    },
    Mutation: {
        createPeso: (parent, { peso }, context, info) => {
            return Peso_1.default.findOne({ tipo: peso }).then((sucesso) => {
                if (sucesso) {
                    throw new Error("Essa tipo de peso já se encontra registrado.");
                }
                return new Peso_1.default({
                    tipo: peso
                }).save().then((sucesso) => {
                    if (!sucesso) {
                        throw new Error('Não conseguimos registrar tipo de peso');
                    }
                    return { tipo: 'Peso registrado com sucesso!' };
                });
            });
        }
    }
};
