"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pesoTypes = `
# User definition type
type Peso {
    _id: ID!
   tipo:String!
}

`;
exports.pesoTypes = pesoTypes;
const pesoQueries = `
pesos: [ Peso! ]!
peso(_id: ID!): Peso
`;
exports.pesoQueries = pesoQueries;
const pesoMutations = `
    createPeso(peso:String!):Peso
`;
exports.pesoMutations = pesoMutations;
