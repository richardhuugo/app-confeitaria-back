"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Produto_1 = require("../../../../models/Produto/Produto");
exports.produtoResolvers = {
    Query: {
        produto: ((parent, { _id }, context, info) => {
            return Produto_1.default.findOne({ _id })
                .then(peso => {
                return peso;
            });
        }),
        // compose(authResolver, verifyTokenResolver)
        produtos: ((parent, { first = 2, offset = 0 }, context, info) => {
            return Produto_1.default.find()
                .populate('empresa')
                .populate('categoria')
                .populate('Peso')
                .limit(first)
                .skip(offset)
                .sort({
                tipo: 'asc'
            })
                .then(sucesso => {
                return sucesso;
            });
        })
    },
    Mutation: {
        createProduto: (parent, args, context, info) => {
            return new Produto_1.default({
                empresa: args.input.empresa,
                imagem: args.input.imagem,
                valor: args.input.valor,
                valor_exibir: args.input.valor_exibir,
                descricao: args.input.descricao,
                categoria: args.input.categoria,
                nome: args.input.nome,
                peso: args.input.peso,
                tp_peso: args.input.tp_peso
            }).save().then((sucesso) => {
                if (!sucesso) {
                    throw new Error('Não conseguimos registrar o produto');
                }
                return sucesso;
            });
        }
    }
};
