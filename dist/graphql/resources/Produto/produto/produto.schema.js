"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const produtoTypes = `
# User definition type
type Produto {
    _id: ID!
    empresa:Empresa!
    imagem:[String!]!
    valor: String!
    valor_exibir:String!
    descricao:String!
    categoria:Categoria!
    nome:String!
    peso:String!
    tp_peso:Peso!
    status:String!   
}

input CreateProdutoEmpresa {
    empresa:Empresa!
    imagem:[String!]!
    valor: String!
    valor_exibir:String!
    descricao:String!
    categoria:Categoria!
    nome:String!
    peso:String!
    tp_peso:Peso!
}
`;
exports.produtoTypes = produtoTypes;
const produtoQueries = `
produtos(first: Int, offset: Int): [ Produto! ]!
produto(_id: ID!): Produto
`;
exports.produtoQueries = produtoQueries;
const produtoMutations = `
    createProduto(input:CreateProdutoEmpresa!):Produto
`;
exports.produtoMutations = produtoMutations;
