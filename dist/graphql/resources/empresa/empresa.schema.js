"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const empresaTypes = `
    type Empresa {
        nickname:String!
        nome: String!
        documento:Documento!
        descricao:String!
        administrador:User!
        status:String!   
    }

    input InputCreateEmpresa {
        nickname:String!
        nome: String!       
        descricao:String!        
    }

    input InputCreateAdm {
        nome:String!
        sobrenome:String!
        email:String!  
        senha:String!   
    }
    input InputCreateDocumento {
        documento:String!
        tipo:String!
    }
`;
exports.empresaTypes = empresaTypes;
const empresaQueries = `
empresas(first: Int, offset: Int): [ Empresa! ]!
empresa(_id: ID!): Empresa

`;
exports.empresaQueries = empresaQueries;
const empresaMutations = `
createEmpresa(input: InputCreateEmpresa!, administrador:InputCreateAdm, documento:InputCreateDocumento!  ): Empresa
updateUser(_id: ID!, input: UserUpdateInput!): User

`;
exports.empresaMutations = empresaMutations;
