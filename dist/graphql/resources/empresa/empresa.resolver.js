"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../../models/User/User");
const bcryptjs_1 = require("bcryptjs");
const Documento_1 = require("../../../models/Empresa/Documento");
const Perfil_1 = require("../../../models/User/Perfil");
const constants_1 = require("../../../config/constants");
const Empresa_1 = require("../../../models/Empresa/Empresa");
exports.empresaResolvers = {
    //
    Query: {},
    Mutation: {
        createEmpresa: (parent, args, context, info) => {
            return User_1.default.findOne({ email: args.administrador.email }).then((sucesso) => {
                if (sucesso) {
                    throw new Error("Existe um usuario registrado com esse email.");
                }
                Documento_1.default.findOne({ documento: args.documento.documento }).then(documento => {
                    if (documento)
                        throw new Error("Documento já registrado no sistema." + documento);
                    return new Documento_1.default({
                        documento: args.documento.documento,
                        tipo: args.documento.tipo
                    }).save().then(documentoSaved => {
                        if (!documentoSaved) {
                            throw new Error("Não conseguimos concluir o cadastro, documento não salvo." + documentoSaved);
                        }
                        return Perfil_1.default.findOne({ perfil: constants_1.EMPRESA }).then(perfil => {
                            // if(!perfil){
                            throw new Error("Não conseguimos concluir o cadastro, perfil nao encontrado." + perfil);
                            //}
                            const salt = bcryptjs_1.genSaltSync();
                            const password = bcryptjs_1.hashSync(args.administrador.senha, salt);
                            return new User_1.default({
                                nome: 'Administrador',
                                sobrenome: args.input.nome,
                                email: args.administrador.email,
                                senha: password,
                                perfil: perfil._id
                            }).save(user => {
                                if (!user) {
                                    throw new Error("Não conseguimos concluir o cadastro, administrador nao criado.");
                                }
                                return new Empresa_1.default({
                                    nickname: args.input.nickname,
                                    nome: args.input.nome,
                                    documento: documento._id,
                                    descricao: args.input.descricao,
                                    administrador: user._id
                                }).save(empresa => {
                                    if (!empresa) {
                                        throw new Error("Não conseguimos concluir o cadastro, empresa nao criada.");
                                    }
                                    return empresa;
                                });
                            });
                        });
                    });
                });
            });
        }
    }
};
