"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../../models/User/User");
const utils_1 = require("../../../config/utils");
const constants_1 = require("../../../config/constants");
const Empresa_1 = require("../../../models/Empresa/Empresa");
const jwt = require("jsonwebtoken");
exports.tokenResolvers = {
    Mutation: {
        createToken: (parent, { email, senha }, context) => {
            return User_1.default.findOne({ email, status: 'A' }).populate('perfil').then((user) => {
                let errorMessage = "Unauthorized, wrong email or password";
                if (!user || !utils_1.isPassword(user.senha, senha)) {
                    throw new Error(errorMessage);
                }
                const dados = user.toObject();
                if (user.perfil.perfil === constants_1.EMPRESA) {
                    return Empresa_1.default.findOne({ administrador: user._id, status: 'A' })
                        .populate('documento')
                        .then(empresa => {
                        if (!empresa) {
                            throw new Error(errorMessage);
                        }
                        dados.empresa = empresa;
                        const token = jwt.sign(dados, process.env.KEY_SERVER.trim(), {
                            expiresIn: "2h"
                        });
                        return { token, perfil: user.perfil.perfil };
                    });
                }
            });
        }
    }
};
