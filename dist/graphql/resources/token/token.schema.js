"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tokenTypes = `
    type Token {
        token:String!
        perfil:String!
    }
`;
exports.tokenTypes = tokenTypes;
const tokenMutations = `
    createToken(email:String!, senha:String!):Token
`;
exports.tokenMutations = tokenMutations;
