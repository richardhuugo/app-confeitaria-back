"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tools_1 = require("graphql-tools");
const query_1 = require("./query");
const mutation_1 = require("./mutation");
const lodash_1 = require("lodash");
const user_schema_1 = require("./resources/user/user.schema");
const token_schema_1 = require("./resources/token/token.schema");
const user_resolvers_1 = require("./resources/user/user.resolvers");
const token_resolvers_1 = require("./resources/token/token.resolvers");
const perfil_schema_1 = require("./resources/perfil/perfil.schema");
const perfil_resolvers_1 = require("./resources/perfil/perfil.resolvers");
const categoria_schema_1 = require("./resources/Produto/categoria/categoria.schema");
const categoria_resolver_1 = require("./resources/Produto/categoria/categoria.resolver");
const peso_resolver_1 = require("./resources/Produto/peso/peso.resolver");
const peso_schema_1 = require("./resources/Produto/peso/peso.schema");
const empresa_resolver_1 = require("./resources/empresa/empresa.resolver");
const empresa_schema_1 = require("./resources/empresa/empresa.schema");
const documento_resolver_1 = require("./resources/documento/documento.resolver");
const documento_schema_1 = require("./resources/documento/documento.schema");
const resolvers = lodash_1.merge(perfil_resolvers_1.perfillResolvers, token_resolvers_1.tokenResolvers, user_resolvers_1.userResolvers, categoria_resolver_1.categoriaResolvers, peso_resolver_1.pesoResolvers, empresa_resolver_1.empresaResolvers, documento_resolver_1.documentoResolvers);
const SchemaDefinition = `
    type Schema {
        query: Query
        mutation: Mutation
    }
`;
exports.default = graphql_tools_1.makeExecutableSchema({ typeDefs: [
        SchemaDefinition,
        query_1.Query,
        mutation_1.Mutation,
        documento_schema_1.documentoTypes,
        empresa_schema_1.empresaTypes,
        peso_schema_1.pesoTypes,
        perfil_schema_1.perfilTypes,
        token_schema_1.tokenTypes,
        user_schema_1.userTypes,
        categoria_schema_1.categoriaTypes
    ],
    resolvers
});
