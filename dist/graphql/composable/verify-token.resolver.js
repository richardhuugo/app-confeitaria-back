"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
exports.verifyTokenResolver = (resolver) => {
    return (parent, args, context, info) => {
        const token = context.authorization ? context.authorization.split(' ')[1] : undefined;
        console.log(token);
        return jwt.verify(token.trim(), process.env.KEY_SERVER.trim(), (error, decoded) => {
            if (!error) {
                return resolver(parent, args, context, info);
            }
            throw new Error(`${error.name}: ${error.message}`);
        });
    };
};
