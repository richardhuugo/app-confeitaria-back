"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./resources/user/user.schema");
const perfil_schema_1 = require("./resources/perfil/perfil.schema");
const categoria_schema_1 = require("./resources/Produto/categoria/categoria.schema");
const peso_schema_1 = require("./resources/Produto/peso/peso.schema");
const empresa_schema_1 = require("./resources/empresa/empresa.schema");
const documento_schema_1 = require("./resources/documento/documento.schema");
const Query = `
    type Query {
        ${documento_schema_1.documentoQueries}
        ${empresa_schema_1.empresaQueries}
        ${peso_schema_1.pesoQueries}
        ${categoria_schema_1.categoriaQueries}
        ${perfil_schema_1.perfilQueries}
       ${user_schema_1.userQueries}
    }
`;
exports.Query = Query;
