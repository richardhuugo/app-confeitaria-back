"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./resources/user/user.schema");
const token_schema_1 = require("./resources/token/token.schema");
const perfil_schema_1 = require("./resources/perfil/perfil.schema");
const categoria_schema_1 = require("./resources/Produto/categoria/categoria.schema");
const peso_schema_1 = require("./resources/Produto/peso/peso.schema");
const empresa_schema_1 = require("./resources/empresa/empresa.schema");
const documento_schema_1 = require("./resources/documento/documento.schema");
const Mutation = `
    type Mutation {
        ${documento_schema_1.documentoMutations}
        ${empresa_schema_1.empresaMutations}
        ${peso_schema_1.pesoMutations}
        ${categoria_schema_1.categoriaMutations}
        ${perfil_schema_1.perfilMutations}
        ${token_schema_1.tokenMutations}
       ${user_schema_1.userMutations}
    }
`;
exports.Mutation = Mutation;
