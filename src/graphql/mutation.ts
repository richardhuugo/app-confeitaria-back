import { userMutations} from './resources/user/user.schema';
import {tokenMutations} from './resources/token/token.schema';
import { perfilMutations } from './resources/perfil/perfil.schema';
import { categoriaMutations } from './resources/Produto/categoria/categoria.schema';
import { pesoMutations } from './resources/Produto/peso/peso.schema';
import { empresaMutations } from './resources/empresa/empresa.schema';
import { documentoMutations } from './resources/documento/documento.schema';

const Mutation = `
    type Mutation {
        ${documentoMutations}
        ${empresaMutations}
        ${pesoMutations}
        ${categoriaMutations}
        ${perfilMutations}
        ${tokenMutations}
       ${userMutations}
    }
`;

export {
    Mutation
}   