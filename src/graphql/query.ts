import { userQueries} from './resources/user/user.schema';
import { perfilQueries } from './resources/perfil/perfil.schema';
import { categoriaQueries } from './resources/Produto/categoria/categoria.schema';
import { pesoQueries } from './resources/Produto/peso/peso.schema';
import { empresaQueries } from './resources/empresa/empresa.schema';
import { documentoQueries } from './resources/documento/documento.schema';

const Query = `
    type Query {
        ${documentoQueries}
        ${empresaQueries}
        ${pesoQueries}
        ${categoriaQueries}
        ${perfilQueries}
       ${userQueries}
    }
`;

export {
    Query
}