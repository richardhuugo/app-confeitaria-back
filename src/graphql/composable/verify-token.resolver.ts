import { ComposableResolver } from "./composable.resolver";
import { ResolverContext } from "../../interfaces/ResolverContextInterface";
import { GraphQLFieldResolver } from "graphql";
import * as jwt from 'jsonwebtoken'
export const verifyTokenResolver: ComposableResolver<any, ResolverContext> = 
    (resolver: GraphQLFieldResolver<any, ResolverContext>): GraphQLFieldResolver<any, ResolverContext> => {

        return (parent, args, context: ResolverContext, info) => {
            const token: string  = context.authorization ? context.authorization.split(' ')[1] : undefined;
            console.log(token)
                return jwt.verify(token.trim(), process.env.KEY_SERVER.trim(), (error, decoded) => {
                    if(!error){
                        return resolver(parent, args, context, info);
                    }
                    throw new Error(`${error.name}: ${error.message}`);
                })
        };
    };