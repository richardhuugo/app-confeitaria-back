import {makeExecutableSchema} from 'graphql-tools';
import { Query } from './query';
import { Mutation } from './mutation';
import {merge} from 'lodash';

import {userTypes} from './resources/user/user.schema';
import { tokenTypes } from './resources/token/token.schema';

import {userResolvers} from './resources/user/user.resolvers';
import { tokenResolvers } from './resources/token/token.resolvers';
import { perfilTypes } from './resources/perfil/perfil.schema';
import { perfillResolvers } from './resources/perfil/perfil.resolvers';
import { categoriaTypes } from './resources/Produto/categoria/categoria.schema';
import { categoriaResolvers } from './resources/Produto/categoria/categoria.resolver';
import { pesoResolvers } from './resources/Produto/peso/peso.resolver';
import { pesoTypes } from './resources/Produto/peso/peso.schema';
import { empresaResolvers } from './resources/empresa/empresa.resolver';
import { empresaTypes } from './resources/empresa/empresa.schema';
import { documentoResolvers } from './resources/documento/documento.resolver';
import { documentoTypes } from './resources/documento/documento.schema';

const resolvers = merge(perfillResolvers,tokenResolvers,userResolvers,categoriaResolvers, pesoResolvers,empresaResolvers, documentoResolvers)
const SchemaDefinition = `
    type Schema {
        query: Query
        mutation: Mutation
    }
`;
 

export default makeExecutableSchema({typeDefs:[
    SchemaDefinition,
     Query,
     Mutation,
     documentoTypes,
     empresaTypes,     
     pesoTypes,
     perfilTypes,
     tokenTypes,
     userTypes,
     categoriaTypes  
],
resolvers
})