const empresaTypes = `
    type Empresa {
        nickname:String!
        nome: String!
        documento:Documento!
        descricao:String!
        administrador:User!
        status:String!   
    }

    input InputCreateEmpresa {
        nickname:String!
        nome: String!       
        descricao:String!        
    }

    input InputCreateAdm {
        nome:String!
        sobrenome:String!
        email:String!  
        senha:String!   
    }
    input InputCreateDocumento {
        documento:String!
        tipo:String!
    }
`;
 
const empresaQueries = `
empresas(first: Int, offset: Int): [ Empresa! ]!
empresa(_id: ID!): Empresa

`;

const empresaMutations = `
createEmpresa(input: InputCreateEmpresa!, administrador:InputCreateAdm, documento:InputCreateDocumento!  ): Empresa
updateUser(_id: ID!, input: UserUpdateInput!): User

`;

export {
    empresaTypes,
    empresaQueries,
    empresaMutations
}