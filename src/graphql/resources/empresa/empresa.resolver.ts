import {GraphQLResolveInfo} from 'graphql';
import User from '../../../models/User/User';
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import Documento from '../../../models/Empresa/Documento';
import Perfil from '../../../models/User/Perfil';
import { EMPRESA } from '../../../config/constants';
import Empresa from '../../../models/Empresa/Empresa';
  
export const empresaResolvers = {
//
    Query: {
     
    },
    Mutation: {
        createEmpresa:   (parent, args, context, info:GraphQLResolveInfo) => {
                                  
            return  User.findOne({email:args.administrador.email} ).then((sucesso) => {                      
                        if(sucesso){
                            throw new Error("Existe um usuario registrado com esse email.");
                        }   
                        
                        Documento.findOne({documento:args.documento.documento}).then(documento => {
                            if(documento)  throw new Error("Documento já registrado no sistema."+documento);

                            return new Documento({
                                documento:args.documento.documento,
                                tipo:args.documento.tipo
                            }).save().then(documentoSaved => {
                                 if(!documentoSaved){                                    
                                    throw new Error("Não conseguimos concluir o cadastro, documento não salvo."+documentoSaved);
                                 }

    
                                return Perfil.findOne({perfil:EMPRESA}).then(perfil => {
    
                                   // if(!perfil){
                                        throw new Error("Não conseguimos concluir o cadastro, perfil nao encontrado."+perfil);
                                    //}
                                    const salt = genSaltSync();
                                    const password = hashSync(args.administrador.senha, salt);
            
                                    return new User({
                                        nome:'Administrador',
                                        sobrenome:args.input.nome,
                                        email:args.administrador.email ,
                                        senha:  password,
                                        perfil:perfil._id
                                    }).save(user => {
                                        if(!user){
                                            throw new Error("Não conseguimos concluir o cadastro, administrador nao criado.");
                                        }
                                        return new Empresa({
                                            nickname:args.input.nickname,
                                            nome: args.input.nome,
                                            documento:documento._id,
                                            descricao:args.input.descricao,
                                            administrador:user._id
                                        }).save(empresa => {
                                            if(!empresa){
                                                throw new Error("Não conseguimos concluir o cadastro, empresa nao criada.");
                                            }
                                            return empresa
                                        })
                                    })
                                })   
                            }) 
                        })
                        
                                           
                    }) 
 
        }
    }
}