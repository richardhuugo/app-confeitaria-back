import {GraphQLResolveInfo} from 'graphql';
import Peso from '../../../../models/Produto/Peso';

  
export const pesoResolvers = {
    Query: {
        peso:  ((parent, {_id}, context, info:GraphQLResolveInfo) => {

            return  Peso.findOne({_id})            
            .then( peso  => {
                  return peso
            })          
          
        }),
        // compose(authResolver, verifyTokenResolver)
        pesos: ((parent, args, context, info:GraphQLResolveInfo) => {
            return Peso.find ( )                       
            .sort({
                tipo: 'asc'
            })
            .then( peso  => {
                  return peso
            })   
        })
    },
    Mutation: {
        createPeso:   (parent, {peso}, context, info:GraphQLResolveInfo) => {
                                  
            return  Peso.findOne({tipo:peso} ).then((sucesso) => {
                       
                        if(sucesso){
                            throw new Error("Essa tipo de peso já se encontra registrado.");
                        }          
                         
                        return new Peso({
                           tipo:peso
                        }).save().then( (sucesso) => {
                            if(!sucesso){
                                throw new Error('Não conseguimos registrar tipo de peso');
                            }
                            return {tipo:'Peso registrado com sucesso!'}
                        }) 
                    }) 
 
        }
    }
}