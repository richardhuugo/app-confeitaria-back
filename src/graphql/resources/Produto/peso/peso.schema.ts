const pesoTypes  = `
# User definition type
type Peso {
    _id: ID!
   tipo:String!
}

`;
const pesoQueries = `
pesos: [ Peso! ]!
peso(_id: ID!): Peso
`;


const pesoMutations = `
    createPeso(peso:String!):Peso
`;


export {
    pesoTypes,
    pesoQueries,
    pesoMutations
}
