import {GraphQLResolveInfo} from 'graphql';

import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import Categoria from '../../../../models/Produto/Categoria';

  
export const categoriaResolvers = {
    Query: {
        categoria:  ((parent, {_id}, context, info:GraphQLResolveInfo) => {

            return  Categoria.findOne({_id})            
            .then( categoria  => {
                  return categoria
            })          
          
        }),
        // compose(authResolver, verifyTokenResolver)
        categorias: ((parent, args, context, info:GraphQLResolveInfo) => {
            return Categoria.find ( )                       
            .sort({
                tipo: 'asc'
            })
            .then( sucesso  => {
                  return sucesso
            })   
        })
    },
    Mutation: {
        createCategoria:   (parent, {categoria}, context, info:GraphQLResolveInfo) => {
                                  
            return  Categoria.findOne({tipo:categoria} ).then((error, sucesso) => {
                        if(error){
                            throw new Error(error);
                        }
                        if(sucesso){
                            throw new Error("Essa categoria já se encontra registrada.");
                        }          
                         
                        return new Categoria({
                           tipo:categoria
                        }).save().then( (error, sucesso) => {
                            if(error){
                                throw new Error(error);
                            }
                            return {tipo:'Categoria registrada com sucesso!'}
                        }) 
                    }) 
 
        }
    }
}