const categoriaTypes  = `
# User definition type
type Categoria {
    _id: ID!
   tipo:String!
}

`;
const categoriaQueries = `
categorias: [ Categoria! ]!
categoria(_id: ID!): Categoria
`;


const categoriaMutations = `
    createCategoria(categoria:String!):Categoria
`;


export {
    categoriaTypes,
    categoriaQueries,
    categoriaMutations
}
