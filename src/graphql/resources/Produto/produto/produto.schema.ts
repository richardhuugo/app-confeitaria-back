const produtoTypes  = `
# User definition type
type Produto {
    _id: ID!
    empresa:Empresa!
    imagem:[String!]!
    valor: String!
    valor_exibir:String!
    descricao:String!
    categoria:Categoria!
    nome:String!
    peso:String!
    tp_peso:Peso!
    status:String!   
}

input CreateProdutoEmpresa {
    empresa:Empresa!
    imagem:[String!]!
    valor: String!
    valor_exibir:String!
    descricao:String!
    categoria:Categoria!
    nome:String!
    peso:String!
    tp_peso:Peso!
}
`;
const produtoQueries = `
produtos(first: Int, offset: Int): [ Produto! ]!
produto(_id: ID!): Produto
`;


const produtoMutations = `
    createProduto(input:CreateProdutoEmpresa!):Produto
`;


export {
    produtoTypes,
    produtoQueries,
    produtoMutations
}
