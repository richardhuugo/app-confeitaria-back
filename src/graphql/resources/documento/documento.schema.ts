const documentoTypes = `
    type Documento {
        _id:ID!
        documento:String!
        tipo:String!
    }
`;

const documentoQueries = `
documento(_id:String!):Documento!
`;
const documentoMutations = ``;


export {
    documentoTypes,
    documentoQueries,
    documentoMutations
}