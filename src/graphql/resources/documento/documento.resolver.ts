import {GraphQLResolveInfo} from 'graphql';
import Documento from '../../../models/Empresa/Documento';
 
export const documentoResolvers = {

    Query: {
    
        documento: (parent, {_id}, context, info:GraphQLResolveInfo) => {
            return Documento.findOne({_id}).then(( documento) => {
                if(!documento){
                    throw new Error('Nao conseguimos encontrar o documento informado')
                }                
                return documento
            })
        }
    }
    
}