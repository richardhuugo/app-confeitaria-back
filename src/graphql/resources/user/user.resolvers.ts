import {GraphQLResolveInfo} from 'graphql';
import User from '../../../models/User/User';
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import { compose } from '../../composable/composable.resolver';
import { authResolver } from '../../composable/auth.resolver';
import { verifyTokenResolver } from '../../composable/verify-token.resolver';

const prepare = (o) => {
    o._id = o._id.toString()
    return o
  }
  
export const userResolvers = {
//
    Query: {
        users:  ((parent, {first = 2, offset=0 }, context, info:GraphQLResolveInfo) => {

            return  User.find()
            .populate('perfil')
            .select(['nome','sobrenome','email','_id'])
            .limit(first)
            .skip(offset)
            .sort({
                tipo: 'asc'
            })
            .then( sucesso  => {
                  return sucesso
            })          
          
        }),
        user: compose(authResolver, verifyTokenResolver) ((parent, {_id}, context, info:GraphQLResolveInfo) => {
            return User.findOne({_id})
            .populate('perfil')
            .select(['nome','sobrenome','email','_id'])            
            .sort({
                tipo: 'asc'
            })
            .then( sucesso  => {
                  return sucesso
            })   
        })
    },
    Mutation: {
        createUser:   (parent, args, context, info:GraphQLResolveInfo) => {
                                  
            return  User.findOne({email:args.input.email} ).then((error, sucesso) => {
                        if(error){
                            throw new Error(error);
                        }
                        if(sucesso){
                            throw new Error("Existe um usuario registrado com esse email.");
                        }          
                        const salt = genSaltSync();
                        const password = hashSync(args.input.senha, salt);

                        return new User({
                            nome:args.input.nome,
                            sobrenome:args.input.sobrenome,
                            email:args.input.email,
                            senha:password,
                            perfil: args.perfil
                        }).save().then( (error, sucesso) => {
                            if(error){
                                throw new Error(error);
                            }
                            return {message:'Usuario registrado com sucesso!'}
                        }) 
                    }) 
 
        }
    }
}