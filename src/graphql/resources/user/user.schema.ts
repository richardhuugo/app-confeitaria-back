
const userTypes = `
 

# User definition type
type User {
    _id: ID!
    nome:String!
    sobrenome:String!
    email:String!
    senha:String!
    perfil:Perfil!
}

input UserCreateInput {
    nome:String!
    sobrenome:String!
    email:String!  
    senha:String!             
}

input UserUpdateInput {
    nome:String!
    sobrenome:String!
    senha:String!        
}  

`;

const userQueries = `
users(first: Int, offset: Int): [ User! ]!
user(_id: ID!): User

`;

const userMutations = `
createUser(input: UserCreateInput!, perfil:[ID!]! ): User
updateUser(_id: ID!, input: UserUpdateInput!): User

`;

export {
userTypes,
userQueries,
userMutations
}