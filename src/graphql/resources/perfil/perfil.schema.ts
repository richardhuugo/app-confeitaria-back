
const perfilTypes = `
 

# User definition type
type Perfil {
    _id: ID!
    perfil:String!
    status:String!
    
}

input PerfilCreateInput {
    perfil:String!
                
}
 

`;

const perfilQueries = `
perfis(first: Int, offset: Int): [ Perfil! ]!
perfil(_id: ID!): Perfil

`;

const perfilMutations = `
createPerfil(input: PerfilCreateInput!): Perfil
delete(_id: ID!): Perfil

`;

export {
perfilTypes,
perfilQueries,
perfilMutations
}