const tokenTypes  = `
    type Token {
        token:String!
        perfil:String!
    }
`;

const tokenMutations = `
    createToken(email:String!, senha:String!):Token
`;

export {
    tokenTypes,
    tokenMutations
}
