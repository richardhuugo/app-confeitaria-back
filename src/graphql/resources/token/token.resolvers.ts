import User from "../../../models/User/User";
import { genSaltSync, hashSync, compareSync } from 'bcryptjs';
import { isPassword } from "../../../config/utils";
import { EMPRESA } from "../../../config/constants";
import Empresa from "../../../models/Empresa/Empresa";
import * as jwt from 'jsonwebtoken';

export const tokenResolvers = {
    Mutation:{
        createToken: (parent, {email, senha}, context) => {
            return User.findOne({email, status:'A'}) .populate('perfil').then((user) => {
                let errorMessage: string = "Unauthorized, wrong email or password";
                
                if(!user ||  !isPassword(user.senha, senha) ) { throw new Error(errorMessage)}
                const dados = user.toObject();
             
                if(user.perfil.perfil === EMPRESA){
                    
                 return  Empresa.findOne({administrador:user._id, status:'A'})
                                .populate('documento')
                                .then(empresa => {
                  
                       if(!empresa){throw new Error(errorMessage)}

                            dados.empresa =  empresa
                            const token = jwt.sign(dados, process.env.KEY_SERVER.trim(), {
                                     expiresIn: "2h"
                                  }) 
        
                            return {token, perfil: user.perfil.perfil};  
                         
                    })
                
                }
                  
                
            })
        }
    }
};