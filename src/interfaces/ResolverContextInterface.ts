import { AuthUser } from "./AuthUserInterface";

export interface ResolverContext {
    
    authorization?: string;
    user?: AuthUser
    
}