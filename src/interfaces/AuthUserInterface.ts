export interface AuthUser {
    
    decoded?: any;
    id: number;
    email?: string;
}