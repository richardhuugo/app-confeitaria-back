import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Cliente = new Schema({           
    pick:{type:String, required:false},    
    user:{ type: Schema.Types.ObjectId, ref: 'User'},
    custom_id:{type:String, required:false},
    documento:{ type: Schema.Types.ObjectId, ref: 'Documento'} ,
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('Cliente', Cliente);
