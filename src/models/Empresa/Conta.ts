import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Conta = new Schema({            
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},             
    bank_account:{type:String, required:true},
    conta:{type:String, required:true},
    data:{type:String, required:false},
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('Conta', Conta);
