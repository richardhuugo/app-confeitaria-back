import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Seller = new Schema({            
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},    
    recebedor:{type:String, required:true},           
    bank_account:{ type: Schema.Types.ObjectId, ref: 'Conta'},
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('Seller', Seller);
