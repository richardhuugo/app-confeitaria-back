import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Documento = new Schema({        
   
    documento:{type:String, required:true},
    tipo:{type:String, required:true   }
},{
    timestamps:true
});
export default mongoose.model('Documento', Documento);
