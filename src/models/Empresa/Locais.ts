import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Locais = new Schema({           
    empresa:{ type: Schema.Types.ObjectId, ref: 'Documento'},
    endereco:{ type: Schema.Types.ObjectId, ref: 'Endereco'},
},{
    timestamps:true
});
export default mongoose.model('Locais', Locais);
