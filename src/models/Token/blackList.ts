import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let BlackList = new Schema({           
    token:{ type: String, required:true},
},{
    timestamps:true
});
export default mongoose.model('BlackList', BlackList);
