import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Perfil = new Schema({           
    perfil:{ type: String, required:true},
    status:{type:String, default:'A'}
},{
    timestamps:true
});
export default mongoose.model('Perfil', Perfil);
