import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let ProdutoStatus = new Schema({           
      produto:{type:String, required:true},
      descricao:{type:String, required:true}
});
export default mongoose.model('ProdutoStatus', ProdutoStatus);
