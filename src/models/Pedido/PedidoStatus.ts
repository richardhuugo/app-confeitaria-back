import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let PedidoStatus = new Schema({           
      pedido:{type:String, required:true},
      descricao:{type:String, required:true}
});
export default mongoose.model('PedidoStatus', PedidoStatus);
