import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Endereco = new Schema({           
    endereco:{ type: String, required:true},
    
},{
    timestamps:true
});
export default mongoose.model('Endereco', Endereco);
