import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Permission = new Schema({           
    permission:{type:String, required:true},
   
},{
    timestamps:true
});
export default mongoose.model('Permission', Permission);
