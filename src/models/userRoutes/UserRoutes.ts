import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let UserRoutes = new Schema({           
    route:{type:String, required:true},
    user:{ type: Schema.Types.ObjectId, ref: 'User'},
    permission:[{type: Schema.Types.ObjectId, ref: 'Permission'}],    
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('UserRoutes', UserRoutes);
