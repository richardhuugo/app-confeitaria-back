import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let ProdutoRetirada = new Schema({           
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},
    produto:{ type: Schema.Types.ObjectId, ref: 'Produto'},
    endereco:{type:String, required:true},
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('ProdutoRetirada', ProdutoRetirada);
