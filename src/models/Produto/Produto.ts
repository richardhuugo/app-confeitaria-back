import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Produto = new Schema({           
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},
    imagem:[{type:String, required:true}],
    valor:{type:String, required:true},
    valor_exibir:{type:Boolean, default:true},
    pronta_entrega: { type: String, default:"0"}, 
    descricao:{type:String, required:true},
    categoria:[{ type: Schema.Types.ObjectId, ref: 'Categoria'}],
    nome:{type:String, required:true},
    peso:{type:String, required:true},
    entrega:{type:Boolean, default:false},
    tp_peso:{ type: Schema.Types.ObjectId, ref: 'Peso'},
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('Produto', Produto);
