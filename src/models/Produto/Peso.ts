
import * as mongoose from "mongoose";
let Schema = mongoose.Schema;

let Peso = new Schema({        
    tipo:{type:String, required:true},          
    status:{ type:String, default:'A' },   
},{
    timestamps:true
});
export default mongoose.model('Peso', Peso);
