import Empresa from '../../models/Empresa/Empresa';
import Cupom from '../../models/Empresa/Cupom';
import Documento from '../../models/Empresa/Documento';
import User from '../../models/User/User';
import Perfil from '../../models/User/Perfil';
import { CLIENTE, ADMIN, EMPRESA, FUNCIONARIO} from '../../config/constants';
import {sendErrorsFromDB, client} from '../../config/utils';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as pagarme from 'pagarme';
import Seller from '../../models/Empresa/Seller';
import Conta from '../../models/Empresa/Conta';
import { resolve } from 'bluebird';
import ProdutoRetirada from '../../models/Produto/ProdutoRetirada';
import Pedido from '../../models/Pedido/Pedido';


export const registrarEmpresa = async (req, res, next) => {
    const nickname      = req.body.nickname || '';    
    const nome_empresa  = req.body.nome_empresa || '';
    const documento     = req.body.documento || '';    
    const descricao     = req.body.descricao || '';
    const email         = req.body.email || '';
    const senha         = req.body.senha || '';
    const telefone      = req.body.telefone || '';
   
    // realiza a verificação do perfil
    let perfil = await verificarPerfil(  EMPRESA);
    let usuario = await verificarUsuario(email);
    let doc = await verificarDocumento(documento);

    if(!perfil) {res.status(400).send({message:"Perfil não registrado!"});} else 
    
    if(usuario) { res.status(400).send({message:"Usuário existente na plataforma!"})} else 
  
    if(doc) { res.status(400).send({message:"Documento existente na plataforma!"})} else 

    {
        const salt = bcrypt.genSaltSync()
        const passwordHash = bcrypt.hashSync(senha, salt)
    
        const saveUser = new User({
            nome:'',
            sobrenome:'',
            email,
            senha:passwordHash,
            telefone,
            perfil:perfil._id
        });
        const saveDocumento = new Documento({
            documento,
            tipo:'cnpj'
        })
    
        const saveEmpresa = new Empresa({
            nickname,
            nome:nome_empresa,
            documento:saveDocumento._id,
            descricao,
            administrador:saveUser._id,    
        })
        saveUser.save();
        saveDocumento.save();
        saveEmpresa.save();
        return res.status(200).send({message:'Empresa registrada com sucesso! '})
    }
   
  
}

const verificarDocumento  = async (documento) => {
    return await Documento.findOne({documento})
}

const verificarPerfil = async (  perfil) => {
     return await Perfil.findOne({perfil}); 
}
 
const verificarUsuario = async (email) => {
    return await User.findOne({email})
}

export const registrarContaBancaria = async (req, res, next) => {
    const {codigobanco,agencia,agencia_dv,conta ,conta_dv,documento,nome_titular} = req.body;

    const user = req['context']['user'];   
    let cli  = await  client();
   let contaFind = await  Conta.findOne({conta, empresa:user.decoded.empresa._id},async (error, sucess) => {
        if(error){return sendErrorsFromDB(res, error)}
        if(sucess){return res.status(400).send({message:'A conta já está criada!'})}

        let createConta = await cli.bankAccounts.create({
            bank_code: codigobanco,
            agencia: agencia,
            agencia_dv:agencia_dv,
            conta: conta,
            conta_dv: conta_dv,
            legal_name: nome_titular,
            document_number: documento}).catch(error => {return sendErrorsFromDB(res, error.response)}) 
 
           await new Conta({
                empresa:user.decoded.empresa._id,
                bank_account:createConta.id,
                conta:createConta.conta,
                data:JSON.stringify(createConta)
            }).save(error => {
                if(error){return sendErrorsFromDB(res, error)}
                return res.status(200).send({message:"Conta bancaria registrada com sucesso!"})
            }) 
 
                
    })


          
}

export const visualizarSaldo = async  (req, res) => {
    let cli  = await  client();
    const user = req['context']['user']; 
    Seller.findOne({empresa:user.decoded.empresa._id},(error, sucesso ) => {
        if(error){}

        if(!sucesso){
            return res.json({message:"Atualize seus dados e começe a vender seus produtos."})
        }

        cli.balance.find({
            recipientId:  sucesso.recebedor
          }).then(total => {
            return res.status(200).send({saldo:total})
          }).catch(errorvalor => {
            return sendErrorsFromDB(res, errorvalor.response);
          })
    })  
   
}
export const registrarProdutoRetiradaEndereco = (req, res) => {
    const {produto, endereco} = req.body;
    const user = req['context']['user']; 
    new ProdutoRetirada({
        empresa:user.decoded.empresa._id,
        produto,
        endereco        
    }).save(error => {
        if(error){return sendErrorsFromDB(res,error)}

        return res.status(200).send({message:"Endereço de retirada registrado com sucesso"});
    })
}
export const registrarSeller = async (req, res) => {
    const {banco_account} = req.body;
   const user = req['context']['user'];   
  
   let cli  = await  client();

    const seller = await Seller.findOne({empresa:user.decoded.empresa._id});
    if(seller){
        return res.json({message:"Função de atualizar dados bancarios do recebedor não implementada"})
    }else{       
        await registSeller(cli,banco_account, user, res )
    }
    
}
const atualizarSeller = async () => {

}
const registSeller = async (cli, bank_account , user, res) => {

    Conta.findOne({_id:bank_account, empresa:user.decoded.empresa._id},async (error, sucesso ) => {
        if(error){return sendErrorsFromDB(res, error)}
        
        if(!sucesso){return res.status(400).send({message:"Não foi possivel registrar o recebedor, informe uma conta bancaria que esteja vinculado ao seu perfil."})}
        
       let recebedor = await cli.recipients.create({
            bank_account_id: sucesso.bank_account,
            transfer_interval: 'monthly',
            transfer_day: 5,
            transfer_enabled: true
          }).catch(error => {
              return sendErrorsFromDB(res, error.response)
          })

          await new Seller({
            empresa:user.decoded.empresa._id,
            recebedor:recebedor.id,
            bank_account:sucesso._id,                    
            }).save(error => {
                console.log(error)
                if(error){return sendErrorsFromDB(res, error)}
                return res.status(200).send({message:"Recebedor registrado com sucesso!"})
            })

    })
           
}
export const alterarStatusProdutos = async (req, res, next) => {
    const user = req['context']['user'];
    const produto = req.body.produto || ''
    const pedido = req.body.pedido || ''
    const status = req.body.status || ''

    await  Pedido.find({_id:pedido}) 
     .select(['_id','data_entrega'])
    .populate({path:'data_entrega.produto'  }) 
    .exec( async (error, dados) => {
        if(user.perfil != EMPRESA){
            return res.json({})
        }
        await dados.map(async (ped, indexd) => {
         
        await  ped.data_entrega.filter(async  (element, index, array) =>{
            if( new String( element.produto.empresa).valueOf()  != new String(user.decoded.empresa._id).valueOf()  )   {   
                dados.length     =0
                dados.push({error:"Você não tem permissão para alterar um pedido que nao é seu!"}   )            
              }                         
           })  
        
       }); 
       if(dados.length != 0){
        Pedido.findOneAndUpdate({
            'data_entrega.produto':produto
        },{
            '$set':{
                'data_entrega.$.status':status
            }
        },{new:true},(err, documentoAtualizado) => {
            if(err){
                return res.status(400).send(err)
            }
            return res.status(200).send({message:"Status pedido atualizado com sucesso!"})
        })
 
       }else{
           return res.status(400).send(dados)
       }  
    })
}
export const registrarCupom = (req, res, next) => {
    const produtos = req.body.produtos || '';
    const cupom = req.body.cupom || ''    ;
    const date_inicio = req.body.date_inicio || '';
    const date_fim = req.body.date_fim || '';
    const quantidade = req.body.quantidade || '';
    const user = req['context']['user'];
    const empresa = user.empresa._id

    Cupom.findOne({cupom},(error, sucesso) => {
        if(error){
            return sendErrorsFromDB(res, error)
        }
        if(sucesso){
            return res.status(400).send({message:'O cupom informado já está registrado! '})
        }

        new Cupom({
            produtos:JSON.parse(produtos),
            empresa,
            cupom,
            date_inicio,
            date_fim,
            quantidade
        }).save(error => {
            if(error){
                return sendErrorsFromDB(res, error)
            }
            return res.status(200).send({message:'Cupom registrado com sucesso ! '})
        })

    })
    

}

export const adicionarProdutoCupom = (req, res, next) => {

}


