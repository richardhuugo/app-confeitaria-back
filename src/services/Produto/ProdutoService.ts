import Categoria from '../../models/Produto/Categoria';
import Peso from '../../models/Produto/Peso';
import Produto from '../../models/Produto/Produto';
import { sendErrorsFromDB } from '../../config/utils';
const Base64 = require('base-64');

export const registrarProdutos = (req, res) => {
    const details = req['context']['user']
    const imagem = req.body.imagens || '';
    const valor = req.body.valor || '';
    const valor_exibir = req.body.valor_exibir || false;
    const descricao = req.body.descricao || '';
    const categoria = req.body.categoria || '';
    const nome = req.body.nome || '';
    const peso = req.body.peso || '';
    const tp_peso = req.body.tp_peso || '';
    const pronta_entrega = req.body.pronta_entrega || '0';

    const saveProduto = new Produto({
        empresa: details.decoded.empresa._id,
        imagem: JSON.parse(imagem),
        valor,
        valor_exibir,
        pronta_entrega,
        descricao,
        categoria: JSON.parse(categoria),
        nome,
        peso,
        tp_peso
    })

    saveProduto.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error);
        }
        return res.status(200).send({ message: 'Produto registrado com sucesso !.' });
    })

}
export const listarProdutos = (req, res) => {
    var id = (req.query.empresa_id) || ''
    var categorias = (req.query.categorias) || ''


    if (id.length != 0) {
        id = Base64.decode(id.trim())
    }
    if (categorias.length != 0) {

        categorias = Base64.decode(categorias.trim())

    }

    let data = {}
    /**  if (id && categorias) {
  
          data = { empresa: id, 'categoria': { $in: ['5c3cc6e91e35162ee07dbd0'] } }
      }
      if (id) {
  
          data = { empresa: id }
      }
      */
    if (categorias) {
        data = { 'categoria': { $in: [categorias] } }
    }
    Produto.find(data).populate('empresa').populate('categoria').populate('tp_peso').exec((error, sucesso) => {
        if (error) { return res.status(400).send(error) }
        console.log(sucesso)
        return res.json({ produtos: sucesso })
    })
}

export const listarCategorias = (req, res) => {

    Categoria.find({}, (error, categorias) => {
        if (error) {
            return sendErrorsFromDB(res, error);
        }
        return res.json({ categorias })
    });
}


export const registrarCategoria = (req, res) => {
    const categoria = req.body.categoria || '';

    Categoria.findOne({ tipo: categoria }, (error, categoriaFind) => {
        if (error) {
            return sendErrorsFromDB(res, error);
        } else if (categoriaFind) {
            return res.status(400).send({ message: 'Categoria já registrada no sistema.' });
        } else {
            new Categoria({
                tipo: categoria
            }).save(error => {
                if (error) {
                    return sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: 'Categoria registrada com sucesso!' });
            })
        }
    })
}

export const registrarPeso = (req, res) => {
    const peso = req.body.peso || '';

    Peso.findOne({ tipo: peso }, (error, pesoFind) => {
        if (error) {
            return sendErrorsFromDB(res, error);
        } else if (pesoFind) {
            return res.status(200).send({ message: 'O tipo de peso já está registrado!' });
        } else {
            new Peso({ tipo: peso }).save(error => {
                if (error) {
                    return sendErrorsFromDB(res, error);
                }
                return res.status(200).send({ message: 'Tipo de peso registrado com sucesso!' });
            })
        }
    })
}
