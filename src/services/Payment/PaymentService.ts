import Pedido from "../../models/Pedido/Pedido";
import PedidoStatus from "../../models/Pedido/PedidoStatus";
import { sendErrorsFromDB, client } from "../../config/utils";
import Produto from "../../models/Produto/Produto";
import ProdutoStatus from "../../models/Pedido/ProdutoStatus";
import { FAZER, PROCESSANDO, CLIENTE, EMPRESA } from "../../config/constants";
import * as lodash from 'lodash';
import Payment from "../../models/Payment/Payment";
import Empresa from "../../models/Empresa/Empresa";
import { generateCard, generateTransaction } from "./PaymentFunctions";
import CardInfo from "../../models/Payment/CardInfo";
import Seller from "../../models/Empresa/Seller";
import Locais from "../../models/Empresa/Locais";
const _ = lodash;
	

export const realizarPagamento = async (req, res, next) => {
    let cli  = await  client();
    const details = req['context']['user']
    const produtos = JSON.parse(req.body.produtos)  || [];
    let  data_entregas = JSON.parse(req.body.data_entrega) || []
    const card_info = req.body.card_info || []
    const card_id = req.body.card_id || ''
    const save_card = req.body.save_card || false
  
    const produtosArray =[]
    const produtosArray2 =[]
    let total_valor = 0
    let pedidos =[]
    let empresas =[]
    let entregas =[]
    let taxa =[];
    let recebedoresEmpresas =[]
    let recipientes =[]
    let empresaValores =[]
    let enderecos =[]
    let cardResponse =""
    let desconto:number =0;
    let porcentovalor:any = 0;
    await produtos.map(   (produto, index) => { 
        produtosArray2.push(produto.produto)
         produtosArray.push({produto:produto.produto,quantidade:produto.quantidade})
    })
 
    let produtosResponse = await Produto.find({  '_id': { $in :(produtosArray2)}})
    
    await produtosResponse.map( async (produt, index) => { 
        // armazena as informacoes pertinentes a entrega de cada empresa
        entregas.push({empresa:produt.empresa, entrega:produt.entrega, produto:produt._id})
        empresas.push(produt.empresa)
        pedidos.push(produt._id)
       

        let  pp =produtos.find(pp => new String( pp.produto).valueOf()  === new String(produt._id).valueOf() )   
        let  pedidosDesconto =produtosArray.find(pedido => new String( pedido.produto).valueOf()  === new String(produt._id).valueOf() )   
        
        if(pp){
            porcentovalor  = parseFloat(formatMoney(((parseFloat(produt.valor)  * parseInt(pp.quantidade) )  * 2) /100, 2,'.','.') ) 
        }
        if(pedidosDesconto){
            pedidosDesconto.desconto =porcentovalor
        }
                
       if(empresaValores.length !=0){
       
            var target=empresaValores.find(temp=>new String( temp.empresa).valueOf()  === new String(produt.empresa).valueOf())
            if(target){
            
                let dado  = (target.valor) + (parseFloat(produt.valor) * (parseInt(pp.quantidade)))  -porcentovalor   
                target.porcentagem =     target.porcentagem +    porcentovalor
                target.valor =parseFloat(formatMoney(dado, 2,'.','.') )
            }else{                               
                empresaValores.push({valor:parseFloat(formatMoney( (parseFloat(produt.valor) * parseInt(pp.quantidade)  -porcentovalor ), 2,'.','.') ), empresa:produt.empresa ,porcentagem: porcentovalor })  
            }     
        }else{          
                empresaValores.push({valor: parseFloat(formatMoney((parseFloat(produt.valor) * parseInt(pp.quantidade)  -porcentovalor ), 2,'.','.') ) , empresa:produt.empresa,porcentagem:porcentovalor  })                                   
        }
         
        desconto +=porcentovalor ;
        total_valor +=(parseFloat(produt.valor)  * parseInt(pp.quantidade) )
    })
  
   // return res.json({empresaValores,total_valor,desconto,produtosArray})
      
    let produtoStatus = await ProdutoStatus.findOne({produto:FAZER});    
    if(!produtoStatus  ){new ProdutoStatus({produto:FAZER, descricao:"Produto em fabricacao"}).save()}
    let pedidoStatus = await PedidoStatus.findOne({pedido:PROCESSANDO})
    if(!pedidoStatus  ){new PedidoStatus({pedido:PROCESSANDO, descricao:"Pedido está sendo processado"}).save()}
   

    data_entregas.map((data, index) => {
        enderecos.push( data.endereco )
        let dd = data.data.split("/")
        let hh  = data.horario.split(":")        
        let objt = {            
            date: new Date(`${dd[2]}-${dd[1]}-${dd[0]}T${hh[0]}:${hh[1]}:00.000Z`),            
            status: produtoStatus._id            
        };
        _.merge(data,objt )
    })
    
   let locais = await  Locais.find({'endereco' : { $all :enderecos }})
   
      // verifica as taxas de entrega de cada empresa 
    let empresaTaxa = await Empresa.find({ '_id': { $in:  (empresas)}}) 
    
    empresaTaxa.map((dado, index) => {
        let local = locais.find(ll => new String( ll.empresa).valueOf()  === new String(dado._id).valueOf())
        if(local){
            taxa.push({empresa:dado._id, taxa:dado.taxa_entrega}) ;
        }       
        recebedoresEmpresas.push(dado._id)
    })

   
    let sellers = await Seller.find({'empresa':{ $in :(recebedoresEmpresas)}})
    let sum:number =0;
    sellers.map((seller, index) => {
        var target=empresaValores.find(temp=>new String( temp.empresa).valueOf()  === new String(seller.empresa).valueOf())
        var targetTaxa = taxa.find(tx => new String( tx.empresa).valueOf()  === new String(seller.empresa).valueOf())
        if(target  ){
                       
                    var targetVal=recipientes.find(temp=>new String( temp.recipient_id).valueOf()  === new String(seller.recebedor).valueOf())
                   
                    if(!targetVal){
                      
                        if(targetTaxa){
                           
                             //total_valor +=  parseFloat(targetTaxa.taxa) 
                             sum = target.valor  // + parseFloat(targetTaxa.taxa)                
                        }else{
                            sum = target.valor
                        }                                                
                         // multiplica por 100 para obter valor em centavos
                        let result =  (sum * 100).toString();
                        // remove tudo o que for diferente de numeros
                        let valor = parseInt(result.replace(/\D+/g, ''));                       
                       
                        recipientes.push( {
                            "recipient_id":seller.recebedor,       
                            "amount":valor,
                            "liable": false,
                            "charge_processing_fee": true
                            })
                    }          
        }             
    })

    let valorDescontar =parseFloat(formatMoney(desconto,2,'.','.'));
    let valorRecebedor = total_valor - valorDescontar;
    // valor cobrado pelo serviço
    total_valor += parseFloat("6.50")
   let valorBusiness =   (  formatMoney(parseFloat("6.50") +parseFloat(formatMoney(desconto,2,'.','.')) ,2,'.','.')  ).toString();
   let valor = parseInt(valorBusiness.replace(/\D+/g, ''));        
    recipientes.push( {
        "recipient_id":"re_cjr6fsa2900xr4u6fjljdrx9t",       
        "amount":valor,
        "liable": true,
        "charge_processing_fee": true
        })      
     
   const savePedido = new Pedido({
        produtos:produtosArray,
        valor_total:formatMoney(valorRecebedor,2,'.','.'),
        taxa_servico:formatMoney(valorDescontar + parseFloat("6.50"),2,'.','.'),
        cliente:details.decoded.cliente._id,
        data_entrega:data_entregas,
        status:pedidoStatus._id
    });
  
     
    if(card_id.trim()){
       let card = await  CardInfo.findOne({_id: card_id, cliente:details.decoded.cliente._id, status:'A'})
       if(!card){
           return res.status(400).send({message:"O cartão salvo informado não foi encontrado"});
       }
        cardResponse =card.card
      
    }else{
        let  card = await  cli.cards.create(generateCard(card_info, details.decoded.cliente.custom_id))

        if(!card.valid){
            return res.status(400).send({message:"Informe um cartão válido!"})
        }

        if(save_card){
            await new CardInfo({card:card.id, cliente:details.decoded.cliente._id, last_digits:card.last_digits, brand:card.brand}).save();
        }
        cardResponse =card.id

    }
     
    const customer = await cli.customers.find({ id: details.decoded.cliente.custom_id });
    delete  customer.object ;
    delete  customer.id ;
    delete  customer.document_type ;   
    delete  customer.date_created ;   
    delete  customer.addresses ;
    delete  customer.phones ;
    delete  customer.born_at ;
    delete  customer.gender ;
    delete  customer.document_number ;
    delete  customer.birthday ;
    delete customer.documents[0].id
    delete customer.documents[0].object
    let result =  (parseFloat(formatMoney(total_valor,2,'.','.')) * 100).toString();
    //remove tudo o que for diferente de numeros
    let valorPagartotal = parseInt(result.replace(/\D+/g, ''));  
  
    let transaction = await cli.transactions.create(generateTransaction(valorPagartotal, cardResponse, customer, details.decoded.cliente._id,recipientes ) ).catch(error => {
        return res.json({error})
      }) 

        savePedido.save(error => {
        if(error){
            return sendErrorsFromDB(error, res)
        }
        new Payment({
            pedido:savePedido._id,
            response:JSON.stringify(transaction),
            status_payment:transaction.status,
            payment_id:transaction.id
        }).save(errors => {
            if(errors){
                return res.json(errors)
            }
            return res.json({message:'Pedido realizado com sucesso!'})
        })
    }) 
  
}

export const registrarStatusPedido = (req, res, next) => {
    const pedido = req.body.pedido || '';
    const descricao = req.body.descricao || '';

    PedidoStatus.findOne({pedido},(error, sucesso) => {
        if(error){
            return sendErrorsFromDB(res, error)
        }
        if(sucesso){
            return res.status(400).send({message:"Status do pedido informado já está registrado"});
        }

        new PedidoStatus({pedido, descricao}).save(error =>{
            if(error){
                return sendErrorsFromDB(res, error)
            }
            return res.status(200).send({message:"Status do pedido registrado com sucesso!. "})
        })
    })
}


export const obterPedidos = async (req, res, next) => {
    const user = req['context']['user']
    let obj ={}
    if(user.perfil == CLIENTE){
        obj=  {cliente:user.decoded.cliente._id}
    }
     
    let produtos =[]
 
   await  Pedido.find(obj)
    .populate({path:'produtos.produto', populate:{
        path:'categoria'
    }})
    .populate({
        path:'cliente', 
        select:['user','_id'],
        populate:{
            path:'user',
            select:['nome','sobrenome','email','_id','telefone']
        }
    })
    .exec( async (error, dados) => {
       
            if(user.perfil == EMPRESA){
                //await dados.map( async (dd, index) => {
                await  dados.map(async (prod,index2) => {                     
                    // remover itens que não é da empresa
                    await prod.produtos.map(async (removeItem, index) =>{    
                        
                        
                        let findd = await prod.produtos.find((emp, i) => {
                            return  new String( emp.produto.empresa).valueOf()  != new String(user.decoded.empresa._id).valueOf() 
                        })
                        if(findd){
                           
                            await  dados.find(async (prod,index2) => {                     
                                let i = prod.produtos.indexOf(findd)
                                if (i > -1) {
                                    prod.produtos.splice(i, 1);
                                 }                                  
                            }) 
    
                        }  
                                                         
                    })
                        
                    await prod.produtos.map((emp, i) => {
                       
                        if(new String( emp.produto.empresa).valueOf()  == new String(user.decoded.empresa._id).valueOf() ){
                            produtos.push( emp.produto._id)
                        }                                       
                    })
                                    
                    await  prod.data_entrega.filter(async  (element, index, array) =>{
                         if( new String( element.produto).valueOf()  != new String(produtos[index]).valueOf()  ){    
                            let i = prod.data_entrega.indexOf(element)    
                            
                            if (i > -1) {
                                prod.data_entrega.splice(i, 1) 
                            }                 
                          
                         }                            
                    });
                                                         
                })     
            } 
            
            return res.json(dados)
         
      
       
    })
}
const formatMoney = function (n , c, d, t) {
    var n = n,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i: any = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};