import { CLIENTE } from "../../config/constants";
import Perfil from "../../models/User/Perfil";
import User from "../../models/User/User";
import { sendErrorsFromDB, client } from "../../config/utils";
import Cliente from "../../models/Cliente/Cliente";
import * as bcrypt from 'bcrypt';
import Documento from "../../models/Empresa/Documento";
import * as  jwt from 'jsonwebtoken';

export const registrarCliente = async (req, res, next) => {    
    const email = req.body.email || '';
    const senha = req.body.senha || '';
     

    User.findOne({email},  (error, user) => {
        if(error){
            return sendErrorsFromDB(res, error);
        }

        if(user){
            return res.status(400).send({message:"Existe um usuario registrado com o email e telefone informado."})
        }

        Perfil.findOne({perfil:CLIENTE},(error, perfil) => {
            if(error){
                return sendErrorsFromDB(res, error);
            }

            if(!perfil){ return res.status(400).send({message:"Não conseguimos finalizar o cadastro, entre em contato conosco."})}
            const salt = bcrypt.genSaltSync()
            const passwordHash = bcrypt.hashSync(senha, salt)
            const saveUser = new User({
                nome:'',                
                sobrenome:'',
                email,
                senha:passwordHash,
                telefone:'',
                perfil:perfil._id
            });

         
            saveUser.save(error => {
                if(error){
                    return sendErrorsFromDB(res, error);
                }

              
                return res.status(200).send({message:"Cadastro realizado com sucesso."})
            })

        })
        
    })
}
export const alterarSenhaCliente = async (req, res, next) => {
    const user = req['context']['user'];    
    const senhatual = req.body.senha || '';
    const senhanova = req.body.novasenha || '';
    User.findOne({_id:user.decoded._id},(error, usr) => {
        if(error){  return sendErrorsFromDB(res, error); }
                 
        if(usr && bcrypt.compareSync(senhatual, usr.senha) ){
            const dadosEmpresa = usr.toObject();
            const salt = bcrypt.genSaltSync()
            const passwordHash = bcrypt.hashSync(senhanova, salt)
            usr.set({senha:passwordHash})
            usr.save((errorSave, usrSaved) =>{
                if(errorSave){return sendErrorsFromDB(res, errorSave);}
                
                Cliente.findOne({user:usr._id},(errorcli, sucesso) => {
                    if(errorcli){  return sendErrorsFromDB(res, errorcli); }

                    dadosEmpresa.cliente =  sucesso
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                             expiresIn: "12h"
                          }) 
    
                    return res.status(200).send({token, mensagem: "Senha alterada com sucesso!"})
                })  
            })          

        }else{
            return res.status(400).send({message:"A senha informada não bate com a atual"})
        }
    })
  
}
export const atualizarCliente = async (req, res, next) => {
    const documento = req.body.documento ||''
    const nome = req.body.nome ||''
    const sobrenome = req.body.sobrenome || ''    
    const telefone = req.body.telefone || '';
    const user = req['context']['user'];    

    let  cliente = await  Cliente.findOne({user:user.decoded._id})
    if(cliente){
        await atualizar(nome, sobrenome, telefone,user,  res)
    }else{
         let cli = await client();
         await  novo(cli, nome, sobrenome, documento, user, telefone, res)
    }
  
}
const atualizar = async (nome, sobrenome, telefone, user,  res) => {
    User.findOne({_id:user.decoded._id},(error, success) => {
        if(error){}
        success.set({nome, sobrenome, telefone});
        success.save(errorSave => {
            if(errorSave){return sendErrorsFromDB(res, errorSave);}
            return res.status(200).send({message:"Dados atualizados com sucesso!."})
        })
    })
}
const novo  = async  (cli, nome, sobrenome, documento, user, telefone, res) => {
 
    let doc = await  Documento.findOne({documento})
    if(doc){
     return res.status(400).send({message:"Não foi possivel concluir o cadastro pois o documento já está registrado no sistema."})
    }

    let newDoct =   new Documento({tipo: 'cpf', documento});
    User.findOne({_id:user.decoded._id},(errorUsr, userSucesso) =>{
        if(errorUsr){return sendErrorsFromDB(res, errorUsr);}
        userSucesso.set({nome, sobrenome, telefone})
        userSucesso.save(errorUser => {
            if(errorUser){ return sendErrorsFromDB(res, errorUser);}

            newDoct.save(error => {
                if(error){
                    return sendErrorsFromDB(res, error);
                }
        
                 cli.customers.create({
                        external_id: user.decoded._id,
                        name: nome,
                        type: 'individual',
                        country: 'br',
                        email: user.decoded.email,
                        documents: [
                        {
                            type: 'cpf',
                            number: documento
                        }
                        ],
                        phone_numbers: [telefone]                  
                    }).then(customer => {
                
                    const saveClient = new Cliente({
                            pick:'',
                            user:user.decoded._id,
                            custom_id:customer.id,
                            documento:newDoct._id
                    })
                     saveClient.save(error => {
                        if(error){
                            return sendErrorsFromDB(res, error); 
                        }
                        return res.status(200).send({message:"Cadastro realizado com sucesso!"})
                
                    })  
                    
                }).catch(error => {                    
                    return sendErrorsFromDB(res, error.response);
                })       
        
            });

        });
    })
    
}