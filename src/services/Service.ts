
import * as  jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import * as  lodash  from 'lodash';
import {  CLIENTE, ADMIN, EMPRESA, FUNCIONARIO} from '../config/constants';
import {strToBool,validar} from '../config/validateAcess';
import Perfil from '../models/User/Perfil';
import {sendErrorsFromDB} from '../config/utils';
import Empresa from '../models/Empresa/Empresa';
import User from '../models/User/User';
import UserRoutes from '../models/userRoutes/UserRoutes';
import Permission from '../models/userRoutes/Permission';
import Cliente from '../models/Cliente/Cliente';
const emailRegex = /\S+@\S+\.\S+/
const passwordRegex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/
const _ = lodash;

 
     
export   const loginUser = (req, res, next) => {
    const email =   req.body.email || ''
    const senha =   req.body.senha || ''


    User.findOne({email } )
    .populate('perfil')            
    .exec((error, user) => {                                
        if(error) {
            return sendErrorsFromDB(res, error)
        } else if(user && bcrypt.compareSync(senha, user.senha))   {
            const dadosEmpresa = user.toObject();
            
        //    
            if(user.perfil.perfil === EMPRESA){
                Empresa.findOne({administrador:user._id},(error, sucesso) => {
                    if(error){

                    }
                    dadosEmpresa.empresa =  sucesso
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                             expiresIn: "12h"
                          }) 

                    return res.status(200).send({token, perfil: user.perfil.perfil})
                })
            }
            
            if(user.perfil.perfil === CLIENTE){
                Cliente.findOne({user:user._id},(error, sucesso) => {
                    if(error){

                    }
                    dadosEmpresa.cliente =  sucesso
                    const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                             expiresIn: "12h"
                          }) 

                    return res.status(200).send({token, perfil: user.perfil.perfil})
                })
            }
            
         

                                              
                                                
        }  else {
            return res.status(400).send({ errors: ['Usuário/Senha inválidos'] })
        } 
   
    });
  
}

export const registrarRoute = (req, res, next) => {
    const route = req.body.route || '';
    const user = req.body.user || '';
    const permission = req.body.permissoes || '';

    UserRoutes.findOne({route, user},(error, sucesso) => {
        if(error){
            return sendErrorsFromDB(res, error);
        }

        if(sucesso){
            return res.status(400).send({message:'Rota já registrada.'}); 
        }

        new UserRoutes({
            route,
            user,
            permission
        }).save(error => {
            if(error){
                return sendErrorsFromDB(res, error);
            }
            return res.status(200).send({message:'Rota registrada com sucesso.'}); 
        })


    })
}
export const registrarPermission = (req, res, next) => {

}
 export const registrarPerfil = (req, res) => {
     const perfil = req.body.perfil || '';
    
     Perfil.findOne({perfil},(error, perfilFind) => {
         if(error){
            return sendErrorsFromDB(res, error);
         }else if(perfilFind) {
            return res.status(400).send({message:'Perfil já registrado'});
         }else{            
            new Perfil({
                perfil
            }).save((error) => {
                if(error){
                    return sendErrorsFromDB(res, error);
                }
                return res.status(200).send({message:'Perfil registrado com sucesso'});
            })
         }
     })
 }