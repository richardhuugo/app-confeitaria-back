

const validar = (req, acesso) =>  {
    var perfil =req.decoded.tp_user.tp_user;
    var id =req.decoded
    var verificado = {status:"false", perfil:null, detalhe:null, id:null};
    acesso.map((dados) => {
        if(dados ==perfil){
           verificado.status ="true";
           verificado.perfil =perfil;
           verificado.detalhe =id;                 
           verificado.id =req.decoded._id;    
        }
    })
   
    return  verificado;
   

}

const strToBool = (s: any) =>
{
    // will match one and only one of the string 'true','1', or 'on' rerardless
    // of capitalization and regardless off surrounding white-space.
    //
   let regex=/^\s*(true|1|on)\s*$/i

    return regex.test(s);
}

export {
    strToBool,
    validar
}