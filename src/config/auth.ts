const jwt = require('jsonwebtoken')
import {ADMIN,CLIENTE,EMPRESA,FUNCIONARIO} from '../config/constants'
import UserRoutes from '../models/userRoutes/UserRoutes';
const auth =  (req, res, next) => {
    // CORS preflight request
    if (req.method === 'OPTIONS') {
        next()
    } else {
        const token =   req.headers['authorization']
          
        if (!token) {
            return res.status(403).send({ errors: ['No token provided.'] })
        }
        req['context'] = {};

        jwt.verify(token,  process.env.KEY_SERVER.trim(), function (err, decoded) {
            if (err) {
                return res.status(403).send({
                    errors: ['Failed to authenticate token.']
                })
            } else {
                /**   if(decoded.perfil.perfil != ADMIN){
                    UserRoutes.findOne({route:req.path, user:decoded._id },(error, routes)=> {
                        if(error){
                            return res.status(403).send({
                                errors: ['Failed to authenticate token.']
                            }) 
                        }
                        if(!routes){
                            return res.status(403).send({
                                errors: ['Failed to authenticate token.']
                            }) 
                        }
                        
                        req['context']['user'] = {
                            decoded,
                            id:decoded._id,
                            perfil:decoded.perfil.perfil
                        }                
                        next()

                    })
                }else{
                  
                }   */             
              
                req['context']['user'] = {
                    decoded,
                    id:decoded._id,
                    perfil:decoded.perfil.perfil
                }                
                next() 
            }
        })
    }
}

export default auth