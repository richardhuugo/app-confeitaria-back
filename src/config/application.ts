import * as express from 'express';
import * as graphsqlHTTP from 'express-graphql';
import * as bodyParser from 'body-parser'
import * as queryParser from 'express-query-int'
import * as logger from 'morgan';
import cors from './cors';
import schema from '../graphql/schema';
import { extractJwtMiddleware } from '../middlewares/extract-jwt-middleware';

class Application {
    public express: express.Application;
    
    constructor() {
        this.express = express();
        this.middleware();
    }

    private middleware(): void {
        
        
        this.express.use(logger('dev'));
        this.express.use(bodyParser.urlencoded({ extended: true }))
        this.express.use(bodyParser.json())
        this.express.use(cors)
        this.express.use(queryParser())
        this.express.use(express.static(__dirname + "/public"));
        this.express.use('/graphql', 
        extractJwtMiddleware(),
        graphsqlHTTP((req) => ({
            schema:schema,
            rootValue: global,
            graphiql: process.env.NODE_ENV.trim() === 'development',
            context:req['context']
        })))  
    }
}

export default new Application().express