const express = require('express')
import auth from './auth';
import { registrarPerfil, registrarPermission, registrarRoute } from '../services/Service';
import { registrarEmpresa, registrarCupom, registrarSeller, registrarContaBancaria, alterarStatusProdutos } from '../services/Empresa/EmpresaService';
import { loginUser } from '../services/Service'


import { open } from 'fs';
import { extractJwtMiddleware } from '../middlewares/extract-jwt-middleware';
import { registrarProdutos, registrarPeso, registrarCategoria, listarProdutos, listarCategorias } from '../services/Produto/ProdutoService';
import { registrarCliente, atualizarCliente, alterarSenhaCliente } from '../services/Clientes/ClienteService';
import { registrarStatusPedido, realizarPagamento, obterPedidos } from '../services/Payment/PaymentService';

/**const auth = require('./auth')
const servico = require('../services/service');
const adminService = require('../services/Admin/adminService');
const funcionarioService = require('../services/Funcionario/funcionarioService')
const treinoService = require('../services/Treino/treinoService')
const alunoService = require('../services/Aluno/alunoService');
const empresaService = require('../services/Empresa/empresaService')
 */
const routes = (server) => {

    // ROTAS PROTEGIDAS
    const protectedApi = express.Router()
    server.use('/close', protectedApi)
    protectedApi.use(auth)




    protectedApi.post('/registrar-cupom', registrarCupom);
    protectedApi.post('/registrar-produto', registrarProdutos)
    protectedApi.post('/registrar-status-pedido', registrarStatusPedido)
    protectedApi.post('/realizar-pagamento', realizarPagamento)
    protectedApi.post('/registrar-seller', registrarSeller)
    protectedApi.post('/atualizar-dados', atualizarCliente)
    protectedApi.post('/atualizar-senha', alterarSenhaCliente)
    protectedApi.post('/registrar-conta-bancaria', registrarContaBancaria)
    protectedApi.get('/listar-pedidos', obterPedidos)
    protectedApi.put('/alterar-status-pedido', alterarStatusProdutos)



    // ROTAS ABERTAS
    const openApi = express.Router()
    server.use('/open', openApi)


    openApi.post('/registrar-perfil', registrarPerfil);
    openApi.post('/registrar-empresa', registrarEmpresa);
    openApi.post('/login-user', loginUser);
    openApi.post('/registrar-routes', registrarRoute);
    openApi.post('/registrar-permissoes', registrarPermission);

    openApi.post('/registrar-peso', registrarPeso)
    openApi.post('/registrar-categoria', registrarCategoria)
    openApi.post('/registrar-cliente', registrarCliente)
    openApi.get('/listar-produtos', listarProdutos)
    openApi.get('/listar-categorias', listarCategorias)


}

export default routes