import * as io from 'socket.io';
import * as http from 'http';
import app from './application';
import { normalizePort, onError, onListening } from './utils';

var server = new http.Server(app);
const socketIo = io(server);

const env  = process.env.port || 3000
const port = normalizePort(env);


app.get('/', function(req, res){
    res.redirect('index.html')
});


socketIo.on('connection', function(socket){
    socket.on('stream', function(image){
        socket.broadcast.emit('stream', image);
    });
})


server.listen(port);
server.on('error', onError(server))
server.on('listening', onListening(server));

  
export   {server, app,socketIo}