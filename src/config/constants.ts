const CLIENTE = "Cliente";
const EMPRESA= "Empresa";
const ADMIN= "Administrador";
const FUNCIONARIO= "Funcionario";
const FAZER = "A fazer";
const FORNO = "Forno";
const ASSANDO = "Assando";
const PROCESSANDO = "PROCESSANDO";

export  {
    CLIENTE, ADMIN, EMPRESA, FUNCIONARIO,
    FAZER,PROCESSANDO
}