import { RequestHandler, Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import User from "../models/User/User";
import BlackList from "../models/Token/blackList";

export const extractJwtMiddleware = (): RequestHandler => {
    return (req: Request, res:Response, next: NextFunction): void => {
        let authorization: string = req.get('authorization');
        let token: string = authorization ? authorization.split(' ')[1] : undefined;
        
        req['context'] = {};
        req['context']['authorization'] = authorization;

       
        if(!token){return next();}
       

        jwt.verify(token, process.env.KEY_SERVER.trim(), (error, decoded) => {
            if(error){return next();}
            User.findOne({_id:decoded._id}).then(user => {
                if(user){
                     
                    req['context']['user'] = {
                        decoded,
                        id:decoded._id,
                        perfil:decoded.perfil._id
                    }                    
                }
                return next();
            })
        })
        
    }
};